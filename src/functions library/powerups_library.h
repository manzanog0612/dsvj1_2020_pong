#ifndef POWERUPS_LIBRARY_H
#define POWERUPS_LIBRARY_H

#include "elements/elements_properties.h"

using namespace elements_properties;

namespace powerup
{
	namespace powerups_logic
	{
		void powerUpInitialization();

		double getTimeForNextPowerUp();

		POWERUPS getPowerUp();

		Vector2 getPosition();

		void activatePowerUpView();

		bool verifyAllOff();

		bool verifyPowerUpCollision();

		void setOffPosition();

		void setUpPowerUp();

		void activatePowerUp();

		void deactivatePowerUps();

		void deactivatePowerUpsByTime();

		void setTimeForPowerUp();

		void drawPowerUp();
	}
	
	void setUpShield();

	void drawShield();

	void setUpBiggerPaddle();

	void setUpMoreSpeed();

	void multiBallDeclarations();

	void setMultiBallMovement();

	bool checkIfBallsOnGame();

	void drawMultiBalls();

	void addPointToPlayerMultiBall();

	void obstaclesDeclaration();

	void setObstaclesMovement();

	void drawObstacles();

	void invertPadsControls();
}

#endif //POWERUPS_LIBRARY_H