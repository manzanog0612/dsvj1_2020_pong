#ifndef GLOBAL_DRAWING_VARS_LIBRARY_H
#define GLOBAL_DRAWING_VARS_LIBRARY_H

namespace global_drawing_vars
{
	struct Words
	{
		const char* text;
		int posY;
		int  fontSize;
	};

	extern Words title;
	extern Words play;
	extern Words options;
	extern Words rules;
	extern Words credits;
	extern Words exit;

	extern Words playAndArrows;
	extern Words optionsAndArrows;
	extern Words rulesAndArrows;
	extern Words creditsAndArrows;
	extern Words exitAndArrows;

	extern Words controls;
	extern Words paddleColor;
	extern Words ballColor;
	extern Words gameMode;
	extern Words controlsAndArrow;
	extern Words paddleColorAndArrow;
	extern Words ballColorAndArrow;
	extern Words gameModeAndArrow;

	extern int linesInOptionsScreen;
	extern Words optionsText[3];

	extern int firstLine;
	extern int secondLine;
	extern int thirdLine;
	extern int fourthLine;
	extern int fifthLine;
	extern int sixthLine;
	extern int seventhLine;
	extern int eighthLine;

	extern Words pressEnter;

	extern Words p1Up;
	extern Words p1Down;
	extern Words p2Up;
	extern Words p2Down;

	extern Words p1PaddleColorMovement;
	extern Words p2PaddleColorMovement;
	extern Words swatch;

	extern Words playerVSplayer;
	extern Words playerVScomputer;

	extern Words ballColorMovement;

	extern Words rulesText[8];

	extern Words pause;

	extern Words pressPtoContinue;
	extern Words pressPtoPause;

	extern Words thePlayerHasWon;

	extern Words one;
	extern Words two;

	extern Words congratulations;

	extern Words p1RoundsWon;
	extern Words p2RoundsWon;
	extern Words p1RoundsWonNumber;
	extern Words p2RoundsWonNumber;

	extern Words creditsText[4];

	extern Words hyphen;

	extern Words gameVersion;
}

#endif
