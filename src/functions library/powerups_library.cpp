#include "functions library/powerups_library.h"

#include "functions library/global_vars_library.h"
#include "game/scenes/sub_scenes.h"
#include "elements/elements.h"

using namespace global_vars;
using namespace sub_scenes;
using namespace elements;
using namespace elements_properties;
using namespace movement_library;

namespace powerup
{
	namespace powerups_logic
	{
		void powerUpInitialization()
		{
			powerUps[static_cast<int>(POWERUPS::SHIELD)].color = MAGENTA;
			powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].color = GREEN;
			powerUps[static_cast<int>(POWERUPS::MORESPEED)].color = WHITE;
			powerUps[static_cast<int>(POWERUPS::MULTIBALL)].color = ORANGE;
			powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].color = DARKPURPLE;
			powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].color = PINK;
			powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].color = SKYBLUE;

			for (short i = 0; i < static_cast<short>(amountPowerUps); i++)
			{
				powerUps[i].onScreen = false;
			}
		}

		double getTimeForNextPowerUp()
		{
			return timeInGame + 2;
		}

		POWERUPS getPowerUp()
		{
			short aux = GetRandomValue(0, static_cast<short>(amountPowerUps) - 1);
			POWERUPS powerUpSelected = POWERUPS(aux);

			switch (powerUpSelected)
			{
			case POWERUPS::SHIELD:				return POWERUPS::SHIELD;				
			case POWERUPS::BIGGERPADDLE:		return POWERUPS::BIGGERPADDLE;			
			case POWERUPS::MORESPEED:			return POWERUPS::MORESPEED;				
			case POWERUPS::MULTIBALL:			return POWERUPS::MULTIBALL;				
			case POWERUPS::OBSTACLESINSCREEN:	return POWERUPS::OBSTACLESINSCREEN;		
			case POWERUPS::INVERTSPEED:			return POWERUPS::INVERTSPEED;			
			case POWERUPS::INVERTCONTROLS:		return POWERUPS::INVERTCONTROLS;		
			default:							return POWERUPS::SHIELD;
			}
		}

		Vector2 getPosition()
		{
			PowerUps powerUp;

			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_UP)].x = 200;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_UP)].y = 200;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)].x = 400;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)].y = 112;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::RIGHT_UP)].x = 600;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::RIGHT_UP)].y = 200;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_DOWN)].x = 200;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_DOWN)].y = 250;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)].x = 400;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)].y = 337;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::RIGHT_DOWN)].x = 600;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::RIGHT_DOWN)].y = 250;

			switch (POWERUPSPOSITIONS(GetRandomValue(0, static_cast<short>(amountPowerUpsPositions) - 1)))
			{
			case POWERUPSPOSITIONS::LEFT_UP:	return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_UP)];		
			case POWERUPSPOSITIONS::CENTRE_UP:	return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)];		
			case POWERUPSPOSITIONS::RIGHT_UP:	return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::RIGHT_UP)];		
			case POWERUPSPOSITIONS::LEFT_DOWN:	return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_DOWN)];		
			case POWERUPSPOSITIONS::CENTRE_DOWN:return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)];	
			case POWERUPSPOSITIONS::RIGHT_DOWN:	return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::RIGHT_DOWN)];	
			default:							return powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::LEFT_UP)];
			}
		}

		void activatePowerUpView()
		{
			PowerUps powerUp;

			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)].x = 400;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)].y = 112;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)].x = 400;
			powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)].y = 337;

			if (verifyAllOff())
			{
				powerUpPos = getPosition();

				do
				{
					actualPowerUp = getPowerUp();

				} while (((powerUpPos.x == powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)].x     &&
					powerUpPos.y == powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_UP)].y) ||
					(powerUpPos.x == powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)].x   &&
						powerUpPos.y == powerUp.pos[static_cast<int>(POWERUPSPOSITIONS::CENTRE_DOWN)].y)) &&
					actualPowerUp == POWERUPS::OBSTACLESINSCREEN);
			}

			switch (actualPowerUp)
			{
			case POWERUPS::SHIELD:			powerUps[static_cast<int>(POWERUPS::SHIELD)].onScreen = true;			break;
			case POWERUPS::BIGGERPADDLE:	powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].onScreen = true;		break;
			case POWERUPS::MORESPEED:		powerUps[static_cast<int>(POWERUPS::MORESPEED)].onScreen = true;		break;
			case POWERUPS::MULTIBALL:		powerUps[static_cast<int>(POWERUPS::MULTIBALL)].onScreen = true;		break;
			case POWERUPS::OBSTACLESINSCREEN: powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].onScreen = true; break;
			case POWERUPS::INVERTSPEED:		powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].onScreen = true;		break;
			case POWERUPS::INVERTCONTROLS:	powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].onScreen = true;	break;
			default: break;
			}
		}

		bool verifyAllOff()
		{
			for (short i = 0; i < static_cast<short>(amountPowerUps); i++)
			{
				if (powerUps[i].onScreen || powerUps[i].on)
					return false;
			}
			return true;
		}

		bool verifyPowerUpCollision()
		{
			PowerUps powerup;

			return CheckCollisionCircles(ball.center, ball.radius, powerUpPos, powerup.radius);
		}

		void setOffPosition()
		{
			powerUpPos.x = 800;
			powerUpPos.y = 450;
		}

		void setUpPowerUp()
		{
			setOffPosition();

			player1LastTouch = player1.lastTouch;

			switch (actualPowerUp)
			{
			case POWERUPS::SHIELD:
				powerUps[static_cast<int>(POWERUPS::SHIELD)].on = true;	
				powerUpEndingTime = GetTime() + powerUpTimeDuration;
				break;
			case POWERUPS::BIGGERPADDLE:
				powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].on = true;
				break;
			case POWERUPS::MORESPEED:
				powerUps[static_cast<int>(POWERUPS::MORESPEED)].on = true;
				break;
			case POWERUPS::MULTIBALL:
				powerUps[static_cast<int>(POWERUPS::MULTIBALL)].on = true;	
				powerUpEndingTime = GetTime() + powerUpTimeDuration;			
				break;
			case POWERUPS::OBSTACLESINSCREEN:
				powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].on = true;	
				powerUpEndingTime = GetTime() + powerUpTimeDuration;	
				break;
			case POWERUPS::INVERTSPEED:
				powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].on = true;	
				powerUpEndingTime = GetTime() + powerUpTimeDuration;			
				break;
			case POWERUPS::INVERTCONTROLS:
				powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].on = true;	
				powerUpEndingTime = GetTime() + powerUpTimeDuration;		
				break;
			default: break;
			}
		}

		void activatePowerUp()
		{
			switch (actualPowerUp)
			{
			case POWERUPS::SHIELD:
				if (powerUps[static_cast<int>(POWERUPS::SHIELD)].on)
				{
					drawShield();
					setUpShield();
					if (powerUps[static_cast<int>(POWERUPS::SHIELD)].onScreen) 
						powerUps[static_cast<int>(POWERUPS::SHIELD)].onScreen = false;
				}	break;
			case POWERUPS::BIGGERPADDLE:
				if (powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].on)
				{
					setUpBiggerPaddle();
					if (powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].onScreen) 
						powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].onScreen = false;
				}	break;
			case POWERUPS::MORESPEED:
				if (powerUps[static_cast<int>(POWERUPS::MORESPEED)].on)
				{
					setUpMoreSpeed();
					if (powerUps[static_cast<int>(POWERUPS::MORESPEED)].onScreen) 
						powerUps[static_cast<int>(POWERUPS::MORESPEED)].onScreen = false;
				}	break;
			case POWERUPS::MULTIBALL:
				if (powerUps[static_cast<int>(POWERUPS::MULTIBALL)].on)
				{
					setMultiBallMovement();
					drawMultiBalls();
					addPointToPlayerMultiBall();
					if (!powerup::checkIfBallsOnGame()) 
						powerUps[static_cast<int>(POWERUPS::MULTIBALL)].on = false;
					if (powerUps[static_cast<int>(POWERUPS::MULTIBALL)].onScreen) 
						powerUps[static_cast<int>(POWERUPS::MULTIBALL)].onScreen = false;
				}	break;
			case POWERUPS::OBSTACLESINSCREEN:
				if (powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].on)
				{
					obstaclesDeclaration();
					setObstaclesMovement();
					drawObstacles();
					if (powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].onScreen) 
						powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].onScreen = false;
				}	break;
			case POWERUPS::INVERTSPEED:
				if (powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].on)
				{
					invertSpeed = true;
					if (powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].onScreen) 
						powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].onScreen = false;
				}	break;
			case POWERUPS::INVERTCONTROLS:
				if (powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].on)
				{
					if (invertControls)
					{
						invertPadsControls();
						invertControls = false;
						if (powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].onScreen) 
							powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].onScreen = false;
					}
				}break;
			default: break;
			}
		}

		void deactivatePowerUps()
		{
			if (actualPowerUp == POWERUPS::INVERTCONTROLS && powerUps[static_cast<short>(POWERUPS::INVERTCONTROLS)].on)
				invertPadsControls();
			for (short i = 0; i < static_cast<short>(amountPowerUps); i++)
			{
				if (powerUps[i].on) powerUps[i].on = false;
				if (powerUps[i].onScreen) powerUps[i].onScreen = false;
			}
			powerUpTimeDurationSet = false;
			invertSpeed = false;
			setTimeForPowerUp();
			invertControls = true;
			activateMultiBallDeclaration = true;
			obstaclesDeclarated = false;
		}

		void deactivatePowerUpsByTime()
		{
			POWERUPS aux;

			if (powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].on)
				invertPadsControls();
			for (short i = 0; i < static_cast<short>(amountPowerUps); i++)
			{
				aux = POWERUPS(i);
				if (aux != POWERUPS::BIGGERPADDLE && aux != POWERUPS::MORESPEED)
				{
					if (powerUps[i].on) powerUps[i].on = false;
					if (powerUps[i].onScreen) powerUps[i].onScreen = false;
				}
			}
			powerUpTimeDurationSet = false;
			invertSpeed = false;
			setTimeForPowerUp();
			invertControls = true;
			activateMultiBallDeclaration = true;
			obstaclesDeclarated = false;
		}

		void setTimeForPowerUp()
		{
			timeInGame = GetTime();
		}

		void drawPowerUp()
		{
			switch (actualPowerUp)
			{
			case POWERUPS::SHIELD:
				if (powerUps[static_cast<int>(POWERUPS::SHIELD)].onScreen) 
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::SHIELD)].radius,
						powerUps[static_cast<int>(POWERUPS::SHIELD)].color);	
				break;
			case POWERUPS::BIGGERPADDLE:
				if (powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].onScreen) 
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].radius,
						powerUps[static_cast<int>(POWERUPS::BIGGERPADDLE)].color);	
				break;
			case POWERUPS::MORESPEED:
				if (powerUps[static_cast<int>(POWERUPS::MORESPEED)].onScreen)
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::MORESPEED)].radius,
						powerUps[static_cast<int>(POWERUPS::MORESPEED)].color);
				break;
			case POWERUPS::MULTIBALL:
				if (powerUps[static_cast<int>(POWERUPS::MULTIBALL)].onScreen)
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::MULTIBALL)].radius,
						powerUps[static_cast<int>(POWERUPS::MULTIBALL)].color);
				break;
			case POWERUPS::OBSTACLESINSCREEN:
				if (powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].onScreen)
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].radius,
						powerUps[static_cast<int>(POWERUPS::OBSTACLESINSCREEN)].color);
				break;
			case POWERUPS::INVERTSPEED:
				if (powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].onScreen)
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].radius,
						powerUps[static_cast<int>(POWERUPS::INVERTSPEED)].color);
				break;
			case POWERUPS::INVERTCONTROLS:
				if (powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].onScreen)
					DrawCircle(static_cast<int>(powerUpPos.x), static_cast<int>(powerUpPos.y), powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].radius,
						powerUps[static_cast<int>(POWERUPS::INVERTCONTROLS)].color);
				break;
			default:	break;
			}
		}
	}

	void setUpShield()
	{
		const short shieldLimitP1 = static_cast<short>(player1.rec.x);
		const short shieldLimitP2 = static_cast<short>(player2.rec.x + player2.rec.width);

		if (player1LastTouch && (ball.center.x - ball.radius <= shieldLimitP1))
			ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
		if (!player1LastTouch && (ball.center.x + ball.radius >= shieldLimitP2))
			ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
	}

	void drawShield()
	{
		const short shieldLimitP1 = static_cast<short>(player1.rec.x);
		const short shieldLimitP2 = static_cast<short>(player2.rec.x + player2.rec.width);

		const short shieldThickness = 16;

		struct Line
		{
			Vector2 StartPos;
			Vector2 EndPos;
		};

		Line p1Shield;
		Line p2Shield;

		p1Shield.StartPos.x = static_cast<float>(shieldLimitP1 - 8);
		p1Shield.StartPos.y = static_cast<float>(screenLimit.up + contourLineThickness);
		p1Shield.EndPos.x = static_cast<float>(shieldLimitP1 - 8);
		p1Shield.EndPos.y = static_cast<float>(screenLimit.down - contourLineThickness);

		p2Shield.StartPos.x = static_cast<float>(shieldLimitP2 + 8);
		p2Shield.StartPos.y = static_cast<float>(screenLimit.up + contourLineThickness);
		p2Shield.EndPos.x = static_cast<float>(shieldLimitP2 + 8);
		p2Shield.EndPos.y = static_cast<float>(screenLimit.down - contourLineThickness);

		if (player1LastTouch)
		{
			DrawLineEx(p1Shield.StartPos, p1Shield.EndPos, shieldThickness, player1.color);
		}
		if (!player1LastTouch)
		{
			DrawLineEx(p2Shield.StartPos, p2Shield.EndPos, shieldThickness, player2.color);
		}
	}

	void setUpBiggerPaddle()
	{
		if (player1LastTouch)
		{
			player1.rec.height = 150;

			if (player1.rec.y + player1.rec.height > screenLimit.down - contourLineThickness)
				player1.rec.y = screenLimit.down - contourLineThickness - player1.rec.height;
		}

		if (!player1LastTouch)
		{
			player2.rec.height = 150;

			if (player2.rec.y + player2.rec.height > screenLimit.down - contourLineThickness)
				player2.rec.y = screenLimit.down - contourLineThickness - player2.rec.height;
		}
	}

	void setUpMoreSpeed()
	{
		if (player1LastTouch)
			player1.speed = 4;

		if (!player1LastTouch)
			player2.speed = 4;
	}

	void multiBallDeclarations()
	{
		for (short i = 0; i < amountMultiBalls; i++)
		{
			balls[i].center.x = ball.center.x;
			balls[i].center.y = ball.center.y;
			balls[i].state.actual = STATEOFBALL::NOTONCOLISION;
			balls[i].state.previous = STATEOFBALL::NOTONCOLISION;
			balls[i].modifyCenter.addToX = GetRandomValue(1, 2) == 1;
			balls[i].modifyCenter.addToY = GetRandomValue(1, 2) == 1;
			balls[i].speed.x = 2;
			balls[i].speed.y = 2;
			balls[i].color = ball.color;
		}
	}

	void setMultiBallMovement()
	{
		float auxBallCenterX;
		float auxBallRadius;

		if (activateMultiBallDeclaration)
		{
			multiBallDeclarations();
			activateMultiBallDeclaration = false;
		}

		for (short i = 0; i < amountMultiBalls; i++)
		{
			if (balls[i].center.x - balls[i].radius > player1.rec.x + player1.rec.width &&
				balls[i].center.x + balls[i].radius < player2.rec.x)
				balls[i].state.actual = STATEOFBALL::NOTONCOLISION;

			if ((CheckCollisionCircleRec(balls[i].center, balls[i].radius, player1.rec) ||
				CheckCollisionCircleRec(balls[i].center, balls[i].radius, player2.rec)) && 
				balls[i].state.previous != STATEOFBALL::ONCOLISION)
				balls[i].state.actual = STATEOFBALL::ONCOLISION;

			if (balls[i].state.previous == STATEOFBALL::ONCOLISION && balls[i].state.actual == STATEOFBALL::ONCOLISION)
				balls[i].state.actual = STATEOFBALL::HAVECOLISIONED;
			else
				balls[i].state.previous = balls[i].state.actual;

			switch (balls[i].state.actual)
			{
			case STATEOFBALL::ONCOLISION:
				auxBallCenterX = balls[i].center.x;
				auxBallRadius = balls[i].radius;

				if (CheckCollisionCircleRec(balls[i].center, balls[i].radius, player1.rec))
				{
					if (balls[i].center.x - balls[i].radius / 2 < player1.rec.x + player1.rec.width)
					{
						balls[i].modifyCenter.addToY = !balls[i].modifyCenter.addToY;
					}
					else balls[i].modifyCenter.addToX = !balls[i].modifyCenter.addToX;

					player1.lastTouch = true;
					player2.lastTouch = false;
				}
				else if (CheckCollisionCircleRec(balls[i].center, balls[i].radius, player2.rec))
				{
					if (balls[i].center.x + balls[i].radius / 2 > player2.rec.x)
					{
						balls[i].modifyCenter.addToY = !balls[i].modifyCenter.addToY;
					}
					else balls[i].modifyCenter.addToX = !balls[i].modifyCenter.addToX;

					player2.lastTouch = true;
					player1.lastTouch = false;
				}
				break;
			case STATEOFBALL::HAVECOLISIONED:
			case STATEOFBALL::NOTONCOLISION:
				if (balls[i].center.y - balls[i].radius <= screenLimit.up + contourLineThickness ||
					balls[i].center.y + balls[i].radius >= screenLimit.down - contourLineThickness)
				{
					balls[i].modifyCenter.addToY = !balls[i].modifyCenter.addToY;
				}
				if (balls[i].modifyCenter.addToY) balls[i].center.y += balls[i].speed.x;
				else balls[i].center.y -= balls[i].speed.x;
				if (balls[i].modifyCenter.addToX) balls[i].center.x += balls[i].speed.y;
				else balls[i].center.x -= balls[i].speed.y;
				break;
			default:
				break;
			}
		}
	}

	bool checkIfBallsOnGame()
	{
		short aux = 0;

		for (short i = 0; i < amountMultiBalls; i++)
		{
			if (balls[i].center.x - balls[i].radius > screenLimit.left + contourLineThickness &&
				balls[i].center.x + balls[i].radius < screenLimit.right - contourLineThickness)
				aux++;
		}

		return aux == amountMultiBalls;
	}

	void drawMultiBalls()
	{
		for (short i = 0; i < amountMultiBalls; i++)
		{
			DrawCircleV(balls[i].center, balls[i].radius, balls[i].color);
		}
	}

	void addPointToPlayerMultiBall()
	{
		for (short i = 0; i < amountMultiBalls; i++)
		{
			if (balls[i].center.x - balls[i].radius < screenLimit.left + contourLineThickness)
			{
				player2.points++;
				score::resetGame();
			}
			else if (balls[i].center.x + balls[i].radius > screenLimit.right - contourLineThickness)
			{
				player1.points++;
				score::resetGame();
			}
		}
	}

	void obstaclesDeclaration()
	{
		if (!obstaclesDeclarated)
		{
			obstacles[static_cast<int>(OBSTACLES::UP)].width = 26;
			obstacles[static_cast<int>(OBSTACLES::UP)].height = 75;
			obstacles[static_cast<int>(OBSTACLES::UP)].x = (screenWidth - obstacles[static_cast<int>(OBSTACLES::UP)].width) / 2;
			obstacles[static_cast<int>(OBSTACLES::UP)].y = contourLineThickness * 3;

			obstacles[static_cast<int>(OBSTACLES::DOWN)].width = 26;
			obstacles[static_cast<int>(OBSTACLES::DOWN)].height = 75;
			obstacles[static_cast<int>(OBSTACLES::DOWN)].y = screenLimit.down - contourLineThickness * 3 - obstacles[static_cast<int>(OBSTACLES::DOWN)].height;
			obstacles[static_cast<int>(OBSTACLES::DOWN)].x = (screenWidth - obstacles[static_cast<int>(OBSTACLES::DOWN)].width) / 2;

			obstaclesDeclarated = true;
		}
	}

	void setObstaclesMovement()
	{
		float obstaclesSpeed = 1;
		if (obstacles[static_cast<int>(OBSTACLES::UP)].y + obstacles[static_cast<int>(OBSTACLES::UP)].height >= 
			obstacles[static_cast<int>(OBSTACLES::DOWN)].y || obstacles[static_cast<int>(OBSTACLES::UP)].y <= screenLimit.up + contourLineThickness || 
			obstacles[static_cast<int>(OBSTACLES::DOWN)].y + + obstacles[static_cast<int>(OBSTACLES::DOWN)].height >= screenLimit.down - contourLineThickness)
			goToScreenLimits = !goToScreenLimits;

		if (goToScreenLimits)
		{
			obstacles[static_cast<int>(OBSTACLES::UP)].y -= obstaclesSpeed;
			obstacles[static_cast<int>(OBSTACLES::DOWN)].y += obstaclesSpeed;
		}
		else
		{
			obstacles[static_cast<int>(OBSTACLES::UP)].y += obstaclesSpeed;
			obstacles[static_cast<int>(OBSTACLES::DOWN)].y -= obstaclesSpeed;
		}
	}

	void drawObstacles()
	{
		const Color color = VIOLET;

		DrawRectangleRec(obstacles[static_cast<int>(OBSTACLES::UP)], color);
		DrawRectangleRec(obstacles[static_cast<int>(OBSTACLES::DOWN)], color);
	}

	void invertPadsControls()
	{
		int auxP1 = player1.up;
		int auxP2 = player2.up;

		player1.up = player1.down;
		player1.down = auxP1;
		player2.up = player2.down;
		player2.down = auxP2;
	}
}