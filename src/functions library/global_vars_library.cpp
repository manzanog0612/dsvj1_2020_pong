#include "global_vars_library.h"

#include "elements/elements_properties.h"

using namespace elements_properties;
using namespace movement_library;

namespace global_vars
{
	Player player1;
	Player player2;
	Ball ball;
	Ball balls[amountMultiBalls];
	PLACEINGAME currentPlaceInGame;
	PLACEINGAME futurePlaceInGame;
	PLACEINOPTIONS  currentPlaceInOptions;

	bool inGame;
	bool pauseMatch = false;
	bool player1LastTouch;
	double timeInGame;

	bool PvsP = true;
	bool invertSpeed = false;
	bool invertControls = true;
	bool powerUpTimeDurationSet = false;
	bool activateMultiBallDeclaration = true;
	double powerUpTimeDuration = 10;
	double powerUpEndingTime;

	POWERUPS actualPowerUp;
	PowerUps powerUps[static_cast<int>(amountPowerUps)];
	Vector2 powerUpPos;
	Rectangle obstacles[static_cast<int>(amountObtacles)];

	short configOption = 1;
	short paddleColorOptionP1 = 7;
	short paddleColorOptionP2 = 12;
	short ballColorOption = 3;
	short gameModeOption = 1;

	float ballModifier = 1;

	bool goToScreenLimits = true;
	bool obstaclesDeclarated = false;

	bool playingGame = true;
}