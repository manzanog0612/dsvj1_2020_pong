#include "functions library/global_drawing_vars_library.h"

#include <iostream>

#include "raylib.h"

namespace global_drawing_vars
{
	Words title;

	Words play;
	Words options;
	Words rules;
	Words credits;
	Words exit;
	Words playAndArrows;
	Words optionsAndArrows;
	Words rulesAndArrows;
	Words creditsAndArrows;
	Words exitAndArrows;

	Words controls;
	Words paddleColor;
	Words ballColor;
	Words gameMode;
	Words controlsAndArrow;
	Words paddleColorAndArrow;
	Words ballColorAndArrow;
	Words gameModeAndArrow;

	int linesInOptionsScreen = 3;
	Words optionsText[3];

	int firstLine = 0;
	int secondLine = 1;
	int thirdLine = 2;
	int fourthLine = 3;
	int fifthLine = 4;
	int sixthLine = 5;
	int seventhLine = 6;
	int eighthLine = 7;

	Words pressEnter;

	Words p1Up;
	Words p1Down;
	Words p2Up;
	Words p2Down;

	Words p1PaddleColorMovement;
	Words p2PaddleColorMovement;
	Words swatch;

	Words playerVSplayer;
	Words playerVScomputer;

	Words ballColorMovement;

	Words rulesText[8];

	Words pause;

	Words pressPtoContinue;
	Words pressPtoPause;

	Words thePlayerHasWon;

	Words one;
	Words two;

	Words congratulations;

	Words p1RoundsWon;
	Words p2RoundsWon;
	Words p1RoundsWonNumber;
	Words p2RoundsWonNumber;

	Words creditsText[4];

	Words hyphen;

	Words gameVersion;
}
