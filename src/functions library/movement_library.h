#ifndef MOVEMENT_LIBRARY_H
#define MOVEMENT_LIBRARY_H

#include "raylib.h"

namespace movement_library
{
	const int KeyW = KEY_W;
	const int KeyS = KEY_S;

	const int screenWidth = 800;
	const int screenHeight = 450;

	struct ScreenLimit
	{
		const int right = screenWidth;
		const int left = 0;
		const int up = 0;
		const int down = screenHeight;
	};

	const ScreenLimit screenLimit;

	const short contourLineThickness = 26;

	enum class PLACEINGAME { MENU, MATCH, OPTIONS, RULES, CREDITS, EXIT, NONE };
	enum class PLACEINOPTIONS { CONTROLS, PADDLECOLOR, BALLCOLOR, GAMEMODE, NONE };
}

#endif //MOVEMENT_LIBRARY_H