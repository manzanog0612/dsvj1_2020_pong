﻿#include <stdlib.h>
#include "game_library.h"
#include "powerups_library.h"
#include "draw_library.h"

using namespace game;
/*short configOption = 1;
short paddleColorOptionP1 = 7;
short paddleColorOptionP2 = 12;
short gameModeOption = 1;
bool PvsP = true;

bool pauseMatch = false;
double timeInGame;

double powerUpTimeDuration = 10;
double powerUpEndingTime;
bool player1LastTouch;
PowerUps powerUps[amountPowerUps];
Vector2 powerUpPos;
POWERUPS actualPowerUp;
bool activateMultiBallDeclaration = true;
bool powerUpTimeDurationSet = false;
bool invertSpeed = false;
bool invertControls = true;
Rectangle obstacles[amountObtacles];*/

void PlayerMovementDefaultDefinition(Player &player1, Player &player2)
{
	player1.up = KeyW;
	player1.down = KeyS;
	player2.up = KEY_U;
	player2.down = KEY_J;
	player1.color = RED;
	player2.color = BLUE;
}

void FirstElementsDefinitions(Player &player1, Player &player2, Ball &ball)
{
	player1.rec.height = 100;
	player1.rec.width = 26;
	player1.rec.x = 59;
	player1.rec.y = screenHeight / 2 - player1.rec.height / 2;
	player1.speed = 2;

	player2.rec.height = 100;
	player2.rec.width = 26;
	player2.rec.x = 715;
	player2.rec.y = screenHeight / 2 - player2.rec.height / 2;
	player2.speed = 2;

	ball.center.x = screenWidth / 2;
	ball.center.y = screenHeight / 2;
	ball.state.actual = STATEOFBALL::NOTONCOLISION;
	ball.state.previous = STATEOFBALL::NOTONCOLISION;
}

void SetPlayersMovement(Player &player1, Player &player2, Ball ball)
{
	float auxPlayer1 = player1.rec.y;
	float auxPlayer2 = player2.rec.y;

	if (IsKeyPressed(player1.up) || IsKeyDown(player1.up))  auxPlayer1 -= player1.speed;
	if (IsKeyPressed(player1.down) || IsKeyDown(player1.down)) auxPlayer1 += player1.speed;

	if (PvsP)
	{
		if (IsKeyPressed(player2.up) || IsKeyDown(player2.up)) auxPlayer2 -= player2.speed;
		if (IsKeyPressed(player2.down) || IsKeyDown(player2.down))  auxPlayer2 += player2.speed;
	}
	else
	{
		if (ball.center.y < player2.rec.y + player2.rec.height/4) auxPlayer2 -= player2.speed;
		if (ball.center.y > player2.rec.y + player2.rec.height/4) auxPlayer2 += player2.speed;
	}
	// Limits the movement of the rectangles to the screen
	if (auxPlayer1 >= screenLimit.up + contourLineThickness && 
		auxPlayer1 <= screenLimit.down - contourLineThickness - player1.rec.height)
		player1.rec.y = auxPlayer1;
	if (auxPlayer2 >= screenLimit.up + contourLineThickness && 
		auxPlayer2 <= screenLimit.down - contourLineThickness - player2.rec.height)
		player2.rec.y = auxPlayer2;
}

void RandomizeBallDirection(Ball &ball)
{
	ball.modifyCenter.addToX = rand() % 2 + 1 == 1;
	ball.modifyCenter.addToY = rand() % 2 + 1 == 1;
}

void SetBallMovement(Rectangle rec1, Rectangle rec2, Ball &ball)
{
	short auxBallCenterX;
	short auxBallRadius;

	if (ball.center.x - ball.radius > rec1.x + rec1.width &&
		ball.center.x + ball.radius < rec2.x)
		ball.state.actual = STATEOFBALL::NOTONCOLISION;

	if ((CheckCollisionCircleRec(ball.center, ball.radius, rec1) ||
		CheckCollisionCircleRec(ball.center, ball.radius, rec2)) && ball.state.previous != STATEOFBALL::ONCOLISION)
		ball.state.actual = STATEOFBALL::ONCOLISION;

	if (ball.state.previous == STATEOFBALL::ONCOLISION)
		ball.state.actual = STATEOFBALL::HAVECOLISIONED;

	ball.state.previous = ball.state.actual;

	switch (ball.state.actual)
	{
	case STATEOFBALL::ONCOLISION:
		auxBallCenterX = ball.center.x;
		auxBallRadius = ball.radius;

		if (CheckCollisionCircleRec(ball.center, ball.radius, rec1))
		{
			if (ball.center.x - ball.radius / 2 < rec1.x + rec1.width)
			{
				ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
				if (invertSpeed)
					ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
			}
			else
			{
				ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
				if (invertSpeed)
					ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
			}
		}
		else if (CheckCollisionCircleRec(ball.center, ball.radius, rec2))
		{
			if (ball.center.x + ball.radius / 2 > rec2.x)
			{
				ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
				if (invertSpeed)
					ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
			}
			else
			{
				ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
				if (invertSpeed)
					ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
			}
		}
		break;
	case STATEOFBALL::HAVECOLISIONED:
	case STATEOFBALL::NOTONCOLISION:
		if (ball.center.y - ball.radius == screenLimit.up + contourLineThickness || 
			ball.center.y + ball.radius == screenLimit.down - contourLineThickness)
		{
			ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
		}
		if (ball.modifyCenter.addToY) ball.center.y += ball.speed.x;
		else ball.center.y -= ball.speed.x;
		if (ball.modifyCenter.addToX) ball.center.x += ball.speed.y;
		else ball.center.x -= ball.speed.y;
		break;
	default:
		break;
	}
}

void SetLastBallTouch(Player &player1, Player &player2, Ball &ball)
{
	if (CheckCollisionCircleRec(ball.center, ball.radius, player1.rec))
	{
		player1.lastTouch = true;
		player2.lastTouch = false;
	}
	else if (CheckCollisionCircleRec(ball.center, ball.radius, player2.rec))
	{
		player2.lastTouch = true;
		player1.lastTouch = false;
	}
}

bool CheckIfBallOnGame(Ball &ball)
{
	return (ball.center.x - ball.radius > screenLimit.left + contourLineThickness &&
			ball.center.x + ball.radius < screenLimit.right - contourLineThickness);
}

void ResetGame(Player &player1, Player &player2, Ball &ball)
{
	DeactivatePowerUps(player1, player2);
	DeactivatePowerUpsByTime(player1, player2);
	SetTimeForPowerUp();
	FirstElementsDefinitions(player1, player2, ball);
	RandomizeBallDirection(ball);
}

void AddPointToPlayer(Player &player1, Player &player2, Ball &ball)
{
	if (ball.center.x + ball.radius >= screenLimit.right - contourLineThickness)
		player1.points++;
	else player2.points++;
}

void SetScore(Player &player1, Player &player2, Ball &ball)
{
	if (!CheckIfBallOnGame(ball))
	{
		AddPointToPlayer(player1, player2, ball);
		ResetGame(player1, player2, ball);
	}
}

void RestartScore(Player &player1, Player &player2)
{
	player1.points = 0;
	player2.points = 0;
}

void SelectMenuChoiceToHightlight(PLACEINGAME &futurePlaceInGame)
{
	struct Word
	{
		Vector2 startPos;
		Vector2 endPos;
	};

	Vector2 mouse;

	Word play;
	Word options;
	Word rules;
	Word exit;

	mouse.x = GetMouseX();
	mouse.y = GetMouseY();

	play.startPos.x = 335;
	play.endPos.x = 440;
	play.startPos.y = 200;
	play.endPos.y = 249;
	//------------------------
	options.startPos.x = 295;
	options.endPos.x = 480;
	options.startPos.y = 250;
	options.endPos.y = 299;
	//------------------------
	rules.startPos.x = 320;
	rules.endPos.x = 455;
	rules.startPos.y = 300;
	rules.endPos.y = 349;
	//------------------------
	exit.startPos.x = 340;
	exit.endPos.x = 435;
	exit.startPos.y = 350;
	exit.endPos.y = 399;

	if (mouse.y >= play.startPos.y && mouse.y <= play.endPos.y &&
		mouse.x >= play.startPos.x && mouse.x <= play.endPos.x)
	{
		futurePlaceInGame = PLACEINGAME::MATCH;
	}
	else if (mouse.y >= options.startPos.y && mouse.y <= options.endPos.y &&
		mouse.x >= options.startPos.x && mouse.x <= options.endPos.x)
	{
		futurePlaceInGame = PLACEINGAME::OPTIONS;
	}
	else if (mouse.y >= rules.startPos.y && mouse.y <= rules.endPos.y &&
		mouse.x >= rules.startPos.x && mouse.x <= rules.endPos.x)
	{
		futurePlaceInGame = PLACEINGAME::RULES;
	}
	else if (mouse.y >= exit.startPos.y && mouse.y <= exit.endPos.y &&
		mouse.x >= exit.startPos.x && mouse.x <= exit.endPos.x)
	{
		futurePlaceInGame = PLACEINGAME::EXIT;
	}
	else
	{
		futurePlaceInGame = PLACEINGAME::NONE;
	}
}

void SelectOptionsChoiceToHightlight(PLACEINOPTIONS &currentPlaceInOptions)
{
	struct Words
	{
		Vector2 startPos;
		Vector2 endPos;
	};

	Vector2 mouse;

	Words controls;
	Words paddleColor;
	Words gameMode;

	mouse.x = GetMouseX();
	mouse.y = GetMouseY();

	controls.startPos.x = 100;
	controls.endPos.x = 320;
	controls.startPos.y = 150;
	controls.endPos.y = 199;
	//---------------------------
	paddleColor.startPos.x = 100;
	paddleColor.endPos.x = 420;
	paddleColor.startPos.y = 200;
	paddleColor.endPos.y = 249;
	//---------------------------
	gameMode.startPos.x = 100;
	gameMode.endPos.x = 360;
	gameMode.startPos.y = 250;
	gameMode.endPos.y = 299;

	if (mouse.y >= controls.startPos.y && mouse.y <= controls.endPos.y &&
		mouse.x >= controls.startPos.x && mouse.x <= controls.endPos.x)
	{
		currentPlaceInOptions = PLACEINOPTIONS::CONTROLS;
	}
	else if (mouse.y >= paddleColor.startPos.y && mouse.y <= paddleColor.endPos.y &&
		mouse.x >= paddleColor.startPos.x && mouse.x <= paddleColor.endPos.x)
	{
		currentPlaceInOptions = PLACEINOPTIONS::PADDLECOLOR;
	}
	else if (mouse.y >= gameMode.startPos.y && mouse.y <= gameMode.endPos.y &&
		mouse.x >= gameMode.startPos.x && mouse.x <= gameMode.endPos.x)
	{
		currentPlaceInOptions = PLACEINOPTIONS::GAMEMODE;
	}
	else
	{
		currentPlaceInOptions = PLACEINOPTIONS::NONE;
	}
}

bool CheckIfMatchEnded(Player player1, Player player2)
{
	return (player1.points == 10 || player2.points == 10);
}

void AddMatchPointToPlayer(Player &player1, Player &player2)
{
	if (player1.points == 10)
		player1.roundsWon++;
	if (player2.points == 10)
		player2.roundsWon++;
}

void GoToMenu(PLACEINGAME &currentPlaceInGame)
{
	currentPlaceInGame = PLACEINGAME::MENU;
}

void GoToPlaceSelected(PLACEINGAME &currentPlaceInGame, PLACEINGAME &futurePlaceInGame)
{
	currentPlaceInGame = futurePlaceInGame;
}

void SetPaddleP1ColorSelected(Player &player1)
{
	COLOR color = COLOR(paddleColorOptionP1);

	switch (color)
	{
	case COLOR::_GRAY:		player1.color = GRAY;		break;
	case COLOR::_DARKGRAY:	player1.color = DARKGRAY;	break;
	case COLOR::_YELLOW:	player1.color = YELLOW;		break;
	case COLOR::_GOLD:		player1.color = GOLD;		break;
	case COLOR::_ORANGE:	player1.color = ORANGE;		break;
	case COLOR::_PINK:		player1.color = PINK;		break;
	case COLOR::_RED:		player1.color = RED;		break;
	case COLOR::_MAROON:	player1.color = MAROON;		break;
	case COLOR::_GREEN:		player1.color = GREEN;		break;
	case COLOR::_DARKGREEN:	player1.color = DARKGREEN;	break;
	case COLOR::_SKYBLUE:	player1.color = SKYBLUE;	break;
	case COLOR::_BLUE:		player1.color = BLUE;		break;
	case COLOR::_DARKBLUE:	player1.color = DARKBLUE;	break;
	case COLOR::_VIOLET:	player1.color = VIOLET;		break;
	case COLOR::_DARKPURPLE:player1.color = DARKPURPLE;	break;
	case COLOR::_BEIGE:		player1.color = BEIGE;		break;
	case COLOR::_BROWN:		player1.color = BROWN;		break;
	case COLOR::_DARKBROWN:	player1.color = DARKBROWN;	break;
	case COLOR::_WHITE:		player1.color = WHITE;		break;
	case COLOR::_BLACK:		player1.color = BLACK;		break;
	case COLOR::_MAGENTA:	player1.color = MAGENTA;	break;
	case COLOR::_RAYWHITE:	player1.color = RAYWHITE;	break;
	default:
		break;
	}
}

void SetPaddleP2ColorSelected(Player &player2)
{
	COLOR color = COLOR(paddleColorOptionP2);

	switch (color)
	{
	case COLOR::_GRAY:		player2.color = GRAY;		break;
	case COLOR::_DARKGRAY:	player2.color = DARKGRAY;	break;
	case COLOR::_YELLOW:	player2.color = YELLOW;		break;
	case COLOR::_GOLD:		player2.color = GOLD;		break;
	case COLOR::_ORANGE:	player2.color = ORANGE;		break;
	case COLOR::_PINK:		player2.color = PINK;		break;
	case COLOR::_RED:		player2.color = RED;		break;
	case COLOR::_MAROON:	player2.color = MAROON;		break;
	case COLOR::_GREEN:		player2.color = GREEN;		break;
	case COLOR::_DARKGREEN:	player2.color = DARKGREEN;	break;
	case COLOR::_SKYBLUE:	player2.color = SKYBLUE;	break;
	case COLOR::_BLUE:		player2.color = BLUE;		break;
	case COLOR::_DARKBLUE:	player2.color = DARKBLUE;	break;
	case COLOR::_VIOLET:	player2.color = VIOLET;		break;
	case COLOR::_DARKPURPLE:player2.color = DARKPURPLE;	break;
	case COLOR::_BEIGE:		player2.color = BEIGE;		break;
	case COLOR::_BROWN:		player2.color = BROWN;		break;
	case COLOR::_DARKBROWN:	player2.color = DARKBROWN;	break;
	case COLOR::_WHITE:		player2.color = WHITE;		break;
	case COLOR::_BLACK:		player2.color = BLACK;		break;
	case COLOR::_MAGENTA:	player2.color = MAGENTA;	break;
	case COLOR::_RAYWHITE:	player2.color = RAYWHITE;	break;
	default:
		break;
	}
}

void SetUpMovementForGameMode(short maxOption)
{
	if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
	{
		gameModeOption -= 1;
		if (gameModeOption < 1) gameModeOption = maxOption;
	}
	if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
	{
		gameModeOption += 1;
		if (gameModeOption > maxOption) gameModeOption = 1;
	}
}

void SetUpMovementForConfigurations(short maxOption)
{
	if (IsKeyPressed(KEY_LEFT))
	{
		configOption -= 1;
		if (configOption < 1) configOption = maxOption;
	}
	if (IsKeyPressed(KEY_RIGHT)) 
	{
		configOption += 1;
		if (configOption > maxOption) configOption = 1;
	}
}

void SetUpMovementForColorOptions(short maxOption)
{
	if (IsKeyPressed(KEY_A))
	{
		paddleColorOptionP1 -= 1;
		if (paddleColorOptionP1 < 1) paddleColorOptionP1 = maxOption;
	}
	if (IsKeyPressed(KEY_D))
	{
		paddleColorOptionP1 += 1;
		if (paddleColorOptionP1 > maxOption) paddleColorOptionP1 = 1;
	}

	if (IsKeyPressed(KEY_LEFT))
	{
		paddleColorOptionP2 -= 1;
		if (paddleColorOptionP2 < 1) paddleColorOptionP2 = maxOption;
	}
	if (IsKeyPressed(KEY_RIGHT))
	{
		paddleColorOptionP2 += 1;
		if (paddleColorOptionP2 > maxOption) paddleColorOptionP2 = 1;
	}
}

//------------------------------------------------------------------------------
// PowerUps

void PowerUpInitialization()
{
	powerUps[0].color = MAGENTA;
	powerUps[1].color = GREEN;
	powerUps[2].color = WHITE;
	powerUps[3].color = ORANGE;
	powerUps[4].color = DARKPURPLE;
	powerUps[5].color = PINK;
	powerUps[6].color = SKYBLUE;

	for (short i = 0; i < amountPowerUps; i++)
	{
		powerUps[i].onScreen = false;
	}
}

double GetTimeForNextPowerUp()
{
	return timeInGame + 2;
}

POWERUPS GetPowerUp() 
{
	short aux = rand() % amountPowerUps;
	POWERUPS powerUpSelected = POWERUPS(aux);

	switch (powerUpSelected)
	{
	case POWERUPS::SHIELD:				return POWERUPS::SHIELD;				break;
	case POWERUPS::BIGGERPADDLE:		return POWERUPS::BIGGERPADDLE;			break;
	case POWERUPS::MORESPEED:			return POWERUPS::MORESPEED;				break;
	case POWERUPS::MULTIBALL:			return POWERUPS::MULTIBALL;				break;
	case POWERUPS::OBSTACLESINSCREEN:	return POWERUPS::OBSTACLESINSCREEN;		break;
	case POWERUPS::INVERTSPEED:			return POWERUPS::INVERTSPEED;			break;
	case POWERUPS::INVERTCONTROLS:		return POWERUPS::INVERTCONTROLS;		break;
	default:							break;
	}
}

Vector2 GetPosition()
{
	PowerUps powerUp;

	powerUp.pos[0].x = 200;
	powerUp.pos[0].y = 200;
	powerUp.pos[1].x = 400;
	powerUp.pos[1].y = 112;
	powerUp.pos[2].x = 600;
	powerUp.pos[2].y = 200;
	powerUp.pos[3].x = 200;
	powerUp.pos[3].y = 250;
	powerUp.pos[4].x = 400;
	powerUp.pos[4].y = 337;
	powerUp.pos[5].x = 600;
	powerUp.pos[5].y = 250;

	switch (rand() % 6)
	{
	case 0:	return powerUp.pos[0]; break;
	case 1:	return powerUp.pos[1]; break;
	case 2:	return powerUp.pos[2]; break;
	case 3:	return powerUp.pos[3]; break;
	case 4:	return powerUp.pos[4]; break;
	case 5:	return powerUp.pos[5]; break;
	}
}

void ActivateView()
{
	if (VerifyAllOff())
	{
		powerUpPos = GetPosition();
		actualPowerUp = GetPowerUp();
	}

	switch (actualPowerUp)
	{
	case POWERUPS::SHIELD:				powerUps[0].onScreen = true;	break;
	case POWERUPS::BIGGERPADDLE:		powerUps[1].onScreen = true;	break;
	case POWERUPS::MORESPEED:			powerUps[2].onScreen = true;	break;
	case POWERUPS::MULTIBALL:			powerUps[3].onScreen = true;	break;
	case POWERUPS::OBSTACLESINSCREEN:	powerUps[4].onScreen = true;	break;
	case POWERUPS::INVERTSPEED:			powerUps[5].onScreen = true;	break;
	case POWERUPS::INVERTCONTROLS:		powerUps[6].onScreen = true;	break;
	default:							break;
	}
}

bool VerifyAllOff()
{
	for (short i = 0; i < amountPowerUps; i++)
	{
		if (powerUps[i].onScreen || powerUps[i].on)
			return false;
	}
	return true;
}

bool VerifyCollision(Ball ball)
{
	return CheckCollisionCircles(ball.center, ball.radius, powerUpPos, powerUps[0].radius);
}

void SetOffPosition()
{
	powerUpPos.x = 800;
	powerUpPos.y = 450;
}

void SetUp(Player player1)
{
	SetOffPosition();

	player1LastTouch = player1.lastTouch;

	switch (actualPowerUp)
	{
	case POWERUPS::SHIELD:				powerUps[0].on = true;	powerUpEndingTime = GetTime() + powerUpTimeDuration; break;
	case POWERUPS::BIGGERPADDLE:		powerUps[1].on = true; 	break;
	case POWERUPS::MORESPEED:			powerUps[2].on = true;	break;
	case POWERUPS::MULTIBALL:			powerUps[3].on = true;	powerUpEndingTime = GetTime() + powerUpTimeDuration; break;
	case POWERUPS::OBSTACLESINSCREEN:	powerUps[4].on = true;	powerUpEndingTime = GetTime() + powerUpTimeDuration; break;
	case POWERUPS::INVERTSPEED:			powerUps[5].on = true;	powerUpEndingTime = GetTime() + powerUpTimeDuration; break;
	case POWERUPS::INVERTCONTROLS:		powerUps[6].on = true;	powerUpEndingTime = GetTime() + powerUpTimeDuration; break;
	default:							break;
	}
}

void Activate(Player &player1, Player &player2, Ball &ball, Ball balls[amountMultiBalls])
{
	switch (actualPowerUp)
	{
	case POWERUPS::SHIELD:
		if (powerUps[0].on)
		{	
			SetUpShield(player1, player2, ball);
			if (powerUps[0].onScreen) powerUps[0].onScreen = false;
		}	break;
	case POWERUPS::BIGGERPADDLE:		
		if (powerUps[1].on)
		{
			SetUpBiggerPaddle(player1, player2);
			if (powerUps[1].onScreen) powerUps[1].onScreen = false;
		}	break;
	case POWERUPS::MORESPEED:			
		if (powerUps[2].on)
		{
			SetUpMoreSpeed(player1, player2);
			if (powerUps[2].onScreen) powerUps[2].onScreen = false;
		}	break;
	case POWERUPS::MULTIBALL:
		if (powerUps[3].on)
		{
			SetMultiBallMovement(player1, player2, balls);
			DrawMultiBalls(balls);
			AddPointToPlayerMultiBall(player1, player2, ball, balls);
			if (!CheckIfBallsOnGame(balls)) powerUps[3].on = false;
			if (powerUps[3].onScreen) powerUps[3].onScreen = false;
		}	break;
	case POWERUPS::OBSTACLESINSCREEN:		
		if (powerUps[4].on) 
		{
			ObstaclesDeclaration();
			SetBallMovementForObstacle(obstacles[0], obstacles[1], ball);
			DrawObstacles();
			if (powerUps[4].onScreen) powerUps[4].onScreen = false;
		}	break;
	case POWERUPS::INVERTSPEED:			
		if (powerUps[5].on)
		{
			invertSpeed = true;
			if (powerUps[5].onScreen) powerUps[5].onScreen = false;
		}	break;
	case POWERUPS::INVERTCONTROLS:		
		if (powerUps[6].on)
		{
			if (invertControls)
			{
				InvertControls(player1, player2);
				invertControls = false;
				if (powerUps[6].onScreen) powerUps[6].onScreen = false;
			}
		}break;
	default:							break;
	}
}

void SetUpShield(Player player1, Player player2, Ball &ball)
{
	const short shieldLimitP1 = 85;
	const short shieldLimitP2 = 715;

	if (player1LastTouch && (ball.center.x - ball.radius <= shieldLimitP1))
		ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
		
	if (!player1LastTouch && (ball.center.x + ball.radius >= shieldLimitP2))
		ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
}

void SetUpBiggerPaddle(Player &player1, Player &player2)
{
	if (player1LastTouch)
	{
		player1.rec.height = 150;

		if (player1.rec.y + player1.rec.height > screenLimit.down - contourLineThickness)
			player1.rec.y = screenLimit.down - contourLineThickness - player1.rec.height;
	}
		

	if (!player1LastTouch)
	{
		player2.rec.height = 150;

		if (player2.rec.y + player2.rec.height > screenLimit.down - contourLineThickness)
			player2.rec.y = screenLimit.down - contourLineThickness - player2.rec.height;
	}
}

void SetUpMoreSpeed(Player &player1, Player &player2)
{
	if (player1LastTouch)
		player1.speed = 4;

	if (!player1LastTouch)
		player2.speed = 4;
}

void MultiBallDeclarations(Ball balls[amountMultiBalls])
{
	for (short i = 0; i < amountMultiBalls; i++)
	{
		balls[i].center.x = screenWidth / 2;
		balls[i].center.y = screenHeight / 2;
		balls[i].state.actual = STATEOFBALL::NOTONCOLISION;
		balls[i].state.previous = STATEOFBALL::NOTONCOLISION;
		balls[i].modifyCenter.addToX = rand() % 2 + 1 == 1;
		balls[i].modifyCenter.addToY = rand() % 2 + 1 == 1;
		balls[i].speed.x = 1.5;
		balls[i].speed.y = 1.5;
	}
}

void SetMultiBallMovement(Player &player1, Player &player2, Ball balls[amountMultiBalls])
{
	short auxBallCenterX;
	short auxBallRadius;

	if (activateMultiBallDeclaration)
	{
		MultiBallDeclarations(balls);
		activateMultiBallDeclaration = false;
	}

	for (short i = 0; i < amountMultiBalls; i++)
	{
		if (balls[i].center.x - balls[i].radius > player1.rec.x + player1.rec.width &&
			balls[i].center.x + balls[i].radius < player2.rec.x)
			balls[i].state.actual = STATEOFBALL::NOTONCOLISION;

		if ((CheckCollisionCircleRec(balls[i].center, balls[i].radius, player1.rec) ||
			CheckCollisionCircleRec(balls[i].center, balls[i].radius, player2.rec)) && balls[i].state.previous != STATEOFBALL::ONCOLISION)
			balls[i].state.actual = STATEOFBALL::ONCOLISION;

		if (balls[i].state.previous == STATEOFBALL::ONCOLISION)
			balls[i].state.actual = STATEOFBALL::HAVECOLISIONED;

		balls[i].state.previous = balls[i].state.actual;

		switch (balls[i].state.actual)
		{
		case STATEOFBALL::ONCOLISION:
			auxBallCenterX = balls[i].center.x;
			auxBallRadius = balls[i].radius;

			if (CheckCollisionCircleRec(balls[i].center, balls[i].radius, player1.rec))
			{
				if (balls[i].center.x - balls[i].radius / 2 < player1.rec.x + player1.rec.width)
				{
					balls[i].modifyCenter.addToY = !balls[i].modifyCenter.addToY;
				}
				else balls[i].modifyCenter.addToX = !balls[i].modifyCenter.addToX;

				player1.lastTouch = true;
				player2.lastTouch = false;
			}
			else if (CheckCollisionCircleRec(balls[i].center, balls[i].radius, player2.rec))
			{
				if (balls[i].center.x + balls[i].radius / 2 > player2.rec.x)
				{
					balls[i].modifyCenter.addToY = !balls[i].modifyCenter.addToY;
				}
				else balls[i].modifyCenter.addToX = !balls[i].modifyCenter.addToX;

				player2.lastTouch = true;
				player1.lastTouch = false;
			}
			break;
		case STATEOFBALL::HAVECOLISIONED:
		case STATEOFBALL::NOTONCOLISION:
			if (balls[i].center.y - balls[i].radius == screenLimit.up + contourLineThickness ||
				balls[i].center.y + balls[i].radius == screenLimit.down - contourLineThickness)
			{
				balls[i].modifyCenter.addToY = !balls[i].modifyCenter.addToY;
			}
			if (balls[i].modifyCenter.addToY) balls[i].center.y += balls[i].speed.x;
			else balls[i].center.y -= balls[i].speed.x;
			if (balls[i].modifyCenter.addToX) balls[i].center.x += balls[i].speed.y;
			else balls[i].center.x -= balls[i].speed.y;
			break;
		default:
			break;
		}
	}
}

bool CheckIfBallsOnGame(Ball balls[amountMultiBalls])
{
	short aux = 0;

	for (short i = 0; i < amountMultiBalls; i++)
	{
		if (balls[i].center.x - balls[i].radius > screenLimit.left + contourLineThickness &&
			balls[i].center.x + balls[i].radius < screenLimit.right - contourLineThickness);
			aux++;
	}

	return aux == amountMultiBalls;
}

void DrawMultiBalls(Ball balls[amountMultiBalls])
{
	for (short i = 0; i < amountMultiBalls; i++)
	{
		DrawCircleV(balls[i].center, balls[i].radius, balls[i].color);
	}
}

void AddPointToPlayerMultiBall(Player &player1, Player &player2, Ball &ball, Ball balls[amountMultiBalls])
{
	for (short i = 0; i < amountMultiBalls; i++)
	{
		if (balls[i].center.x - balls[i].radius < screenLimit.left + contourLineThickness)
		{
			player2.points++;
			ResetGame(player1, player2, ball);
		}
		else if (balls[i].center.x + balls[i].radius > screenLimit.right - contourLineThickness)
		{
			player1.points++;
			ResetGame(player1, player2, ball);
		}
	}
}

void ObstaclesDeclaration()
{
	obstacles[0].width = 26;
	obstacles[0].height = 75;
	obstacles[0].x = (screenWidth + obstacles[0].width) / 2;
	obstacles[0].y = contourLineThickness * 3;

	obstacles[1].width = 26;
	obstacles[1].height = 75;
	obstacles[1].y = screenLimit.down - contourLineThickness * 3 - obstacles[1].height;
	obstacles[1].x = (screenWidth + obstacles[1].width) / 2;
}

void DrawObstacles()
{
	const Color color = VIOLET;

	DrawRectangleRec(obstacles[0], color);
	DrawRectangleRec(obstacles[1], color);
}

void SetBallMovementForObstacle(Rectangle rec1, Rectangle rec2, Ball &ball)
{
	if (CheckCollisionCircleRec(ball.center, ball.radius, rec1))
	{
		if (ball.center.x - ball.radius == rec1.x + rec1.width || ball.center.x + ball.radius == rec1.x)
			ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
	}
	else if (CheckCollisionCircleRec(ball.center, ball.radius, rec2))
	{
		if (ball.center.x - ball.radius == rec1.x + rec1.width || ball.center.x + ball.radius == rec1.x)
			ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
	}
}

void InvertControls(Player &player1, Player &player2)
{
	int auxP1 = player1.up;
	int auxP2 = player2.up;

	player1.up = player1.down;
	player1.down = auxP1;
	player2.up = player2.down;
	player2.down = auxP2;
}

void DeactivatePowerUps(Player &player1, Player &player2)
{
	if (actualPowerUp == POWERUPS::INVERTCONTROLS && powerUps[6].on)
		InvertControls(player1, player2);
	for (short i = 0; i < amountPowerUps; i++)
	{
		if (powerUps[i].on) powerUps[i].on = false;
		if (powerUps[i].onScreen) powerUps[i].onScreen = false;
	}
	powerUpTimeDurationSet = false;
	invertSpeed = false;
	SetTimeForPowerUp();
	invertControls = true;
	activateMultiBallDeclaration = true;

}

void DeactivatePowerUpsByTime(Player &player1, Player &player2)
{
	POWERUPS aux;

	if (actualPowerUp == POWERUPS::INVERTCONTROLS&& powerUps[6].on)
		InvertControls(player1, player2);
	for (short i = 0; i < amountPowerUps; i++)
	{
		aux = POWERUPS(i);
		if (aux != POWERUPS::BIGGERPADDLE && aux != POWERUPS::MORESPEED)
		{
			if (powerUps[i].on) powerUps[i].on = false;
			if (powerUps[i].onScreen) powerUps[i].onScreen = false;
		}
	}
	powerUpTimeDurationSet = false;
	invertSpeed = false;
	SetTimeForPowerUp();
	invertControls = true;
	activateMultiBallDeclaration = true;
}

void SetTimeForPowerUp()
{
	timeInGame = GetTime();
}

//------------------------------------------------------------------------------
// To draw
//DrawText(const char *text, int posX, int posY, int fontSize, Color color);     

void DrawGameContourn(Color colorChoice)
{
	Color color = colorChoice;

	struct Line
	{
		Vector2 StartPos;
		Vector2 EndPos;
	};

	Line upperLine;
	Line lowerLine;
	Line leftLine;
	Line rightLine;

	upperLine.StartPos.y = screenLimit.up + contourLineThickness / 2;
	upperLine.StartPos.x = screenLimit.left;
	upperLine.EndPos.y = screenLimit.up + contourLineThickness / 2;
	upperLine.EndPos.x = screenLimit.right;
	//--------------------------------------------
	lowerLine.StartPos.y = screenLimit.down - contourLineThickness / 2;
	lowerLine.StartPos.x = screenLimit.left;
	lowerLine.EndPos.y = screenLimit.down - contourLineThickness / 2;
	lowerLine.EndPos.x = screenLimit.right;
	//--------------------------------------------
	leftLine.StartPos.y = screenLimit.up;
	leftLine.StartPos.x = screenLimit.left + contourLineThickness / 2;
	leftLine.EndPos.y = screenLimit.down;
	leftLine.EndPos.x = screenLimit.left + contourLineThickness / 2;
	//--------------------------------------------
	rightLine.StartPos.y = screenLimit.up;
	rightLine.StartPos.x = screenLimit.right - contourLineThickness / 2;
	rightLine.EndPos.y = screenLimit.down;
	rightLine.EndPos.x = screenLimit.right - contourLineThickness / 2;
	//--------------------------------------------

	DrawLineEx(upperLine.StartPos, upperLine.EndPos, contourLineThickness, color);
	DrawLineEx(lowerLine.StartPos, lowerLine.EndPos, contourLineThickness, color);
	DrawLineEx(leftLine.StartPos, leftLine.EndPos, contourLineThickness, color);
	DrawLineEx(rightLine.StartPos, rightLine.EndPos, contourLineThickness, color);
}

void DrawPlayerPoints(Player player1, Player player2)
{
	const short fontSize = 100;
	const short posY = 180;
	const short posXPlayer1 = 296;
	const short posXPlayer2 = 452;
	const Color color = VIOLET;

	switch (player1.points)
	{
	case 0:		DrawText("0", posXPlayer1, posY, fontSize, color);		break;
	case 1:		DrawText("1", posXPlayer1 + 14, posY, fontSize, color);	break;
	case 2:		DrawText("2", posXPlayer1, posY, fontSize, color);		break;
	case 3:		DrawText("3", posXPlayer1, posY, fontSize, color);		break;
	case 4:		DrawText("4", posXPlayer1, posY, fontSize, color);		break;
	case 5:		DrawText("5", posXPlayer1, posY, fontSize, color);		break;
	case 6:		DrawText("6", posXPlayer1, posY, fontSize, color);		break;
	case 7:		DrawText("7", posXPlayer1, posY, fontSize, color);		break;
	case 8:		DrawText("8", posXPlayer1, posY, fontSize, color);		break;
	case 9:		DrawText("9", posXPlayer1, posY, fontSize, color);		break;
	default:	break;
	}

	DrawText("-", 380, posY, fontSize, color);

	switch (player2.points)
	{
	case 0:		DrawText("0", posXPlayer2, posY, fontSize, color);		break;
	case 1:		DrawText("1", posXPlayer2 + 10, posY, fontSize, color);	break;
	case 2:		DrawText("2", posXPlayer2, posY, fontSize, color);		break;
	case 3:		DrawText("3", posXPlayer2, posY, fontSize, color);		break;
	case 4:		DrawText("4", posXPlayer2, posY, fontSize, color);		break;
	case 5:		DrawText("5", posXPlayer2, posY, fontSize, color);		break;
	case 6:		DrawText("6", posXPlayer2, posY, fontSize, color);		break;
	case 7:		DrawText("7", posXPlayer2, posY, fontSize, color);		break;
	case 8:		DrawText("8", posXPlayer2, posY, fontSize, color);		break;
	case 9:		DrawText("9", posXPlayer2, posY, fontSize, color);		break;
	default:	break;
	}
}

void DrawMatchPoint(Player player1, Player player2)
{
	Color color = YELLOW;
	short fontSize = 17;

	struct Words
	{
		Vector2 pos;
		Vector2 numberPos;
	};

	Words P1Rounds;
	Words P2Rounds;

	P1Rounds.pos.x = 105;
	P1Rounds.pos.y = 380;
	P1Rounds.numberPos.x = 143;
	P1Rounds.numberPos.y = P1Rounds.pos.y + fontSize;

	P2Rounds.pos.x = 610;
	P2Rounds.pos.y = 380;
	P2Rounds.numberPos.x = 648;
	P2Rounds.numberPos.y = P2Rounds.pos.y + fontSize;

	DrawText("Rounds Won", P1Rounds.pos.x, P1Rounds.pos.y, fontSize, color);
	DrawText("Rounds Won", P2Rounds.pos.x, P2Rounds.pos.y, fontSize, color);

	DrawText(FormatText("%01i", player1.roundsWon), P1Rounds.numberPos.x, P1Rounds.numberPos.y, fontSize + 13, color);
	DrawText(FormatText("%01i", player2.roundsWon), P2Rounds.numberPos.x, P2Rounds.numberPos.y, fontSize + 13, color);
}

void DrawElements(Player &player1, Player &player2, Ball &ball)
{
	DrawRectangleRec(player1.rec, player1.color);
	DrawRectangleRec(player2.rec, player2.color);
	DrawCircleV(ball.center, ball.radius, ball.color);
}

void DrawGameSpace(Player &player1, Player &player2, Ball &ball)
{
	ClearBackground(PURPLE);
	DrawGameContourn(VIOLET);
	DrawText("Press enter to go back to menu", 190, 30, 26, VIOLET);
	DrawText("Press P to pause the game", 225, 394, 26, VIOLET);
	//DrawMatchPoint(player1, player2);
	DrawPlayerPoints(player1, player2);
	DrawElements(player1, player2, ball);
}

void DrawMenu(PLACEINGAME &futurePlaceInGame)
{
	const Color colorPlaceInMenu = MAGENTA;
	const Color colorInMenu = YELLOW;

	const short fontSize = 50;
	const short arrowsLenght = 90;

	struct Word
	{
		Vector2 pos;
	};
	
	Word play;
	Word options;
	Word rules;
	Word exit;

	play.pos.x = 335;
	play.pos.y = 200;

	options.pos.x = 295;
	options.pos.y = 250;

	rules.pos.x = 320;
	rules.pos.y = 300;

	exit.pos.x = 340;
	exit.pos.y = 350;

	ClearBackground(PURPLE);
	DrawGameContourn(VIOLET);
	DrawText("Ping Pong", 150, 75, 100, colorInMenu);

	switch (futurePlaceInGame)
	{
	case PLACEINGAME::MATCH:
		DrawText("--> Play <--", play.pos.x - arrowsLenght, play.pos.y, fontSize, colorPlaceInMenu);
		DrawText("Options", options.pos.x, options.pos.y, fontSize, colorInMenu);
		DrawText("Rules", rules.pos.x, rules.pos.y, fontSize, colorInMenu);
		DrawText("Exit", exit.pos.x, exit.pos.y, fontSize, colorInMenu);
		break;
	case PLACEINGAME::OPTIONS:
		DrawText("Play", play.pos.x, play.pos.y, fontSize, colorInMenu);
		DrawText("--> Options <--", options.pos.x - arrowsLenght, options.pos.y, fontSize, colorPlaceInMenu);
		DrawText("Rules", rules.pos.x, rules.pos.y, fontSize, colorInMenu);
		DrawText("Exit", exit.pos.x, exit.pos.y, fontSize, colorInMenu);
		break;
	case PLACEINGAME::RULES:
		DrawText("Play", play.pos.x, play.pos.y, fontSize, colorInMenu);
		DrawText("Options", options.pos.x, options.pos.y, fontSize, colorInMenu);
		DrawText("--> Rules <--", rules.pos.x - arrowsLenght, rules.pos.y, fontSize, colorPlaceInMenu);
		DrawText("Exit", exit.pos.x, exit.pos.y, fontSize, colorInMenu);
		break;
	case PLACEINGAME::EXIT:
		DrawText("Play", play.pos.x, play.pos.y, fontSize, colorInMenu);
		DrawText("Options", options.pos.x, options.pos.y, fontSize, colorInMenu);
		DrawText("Rules", rules.pos.x, rules.pos.y, fontSize, colorInMenu);
		DrawText("--> Exit <--", exit.pos.x - arrowsLenght, exit.pos.y, fontSize, colorPlaceInMenu);
		break;
	case PLACEINGAME::NONE:
	default:
		DrawText("Play", play.pos.x, play.pos.y, fontSize, colorInMenu);
		DrawText("Options", options.pos.x, options.pos.y, fontSize, colorInMenu);
		DrawText("Rules", rules.pos.x, rules.pos.y, fontSize, colorInMenu);
		DrawText("Exit", exit.pos.x, exit.pos.y, fontSize, colorInMenu);
		break;
	}
}

void DrawWinnerScreen(Player &player1, Player &player2)
{
	const short colorPosX = 357;
	const short posY = 125;
	const short fontSize = 50;
	const Color color = YELLOW;

	ClearBackground(PURPLE);
	DrawGameContourn(VIOLET);

	DrawText("The player", 63, posY, fontSize, color);
	if (player1.points == 10)
		DrawText(" 1 ", colorPosX + 5, posY, fontSize, player1.color);
	else
		DrawText(" 2 ", colorPosX, posY, fontSize, player2.color);
	DrawText("has won!", 470, posY, fontSize, color);

	DrawText("Congratulations!", 125, 195, 75, color);

	DrawText("Press enter to go back to menu", 150, 325, 30, VIOLET);
}

void DrawRules(Player &player1, Player &player2)
{
	const short posX = 70;
	const short fontSize = 30;
	const Color color = YELLOW;

	ClearBackground(PURPLE);
	DrawGameContourn(VIOLET);

	DrawText("The objetive of the game is to keep the ball", posX, 70, fontSize, color);
	DrawText("on the game without it touching none of the", posX, 100, fontSize, color);
	DrawText("limits of the screen.", posX, 130, fontSize, color);
	DrawText("Controls:", posX, 190, fontSize, color);
	DrawText("-Red player: W (up) and S (down)", posX, 220, fontSize, color);
	DrawText("-Blue player: Up arrow and down arrow", posX, 250, fontSize, color);
	DrawText("The first player to reach 10 points wins!", posX, 280, fontSize, color);
	DrawText("Press enter to go back to menu", 150, 355, 30, VIOLET);
}

void DrawOptions(PLACEINOPTIONS &currentPlaceInOptions)
{
	const Color colorSelected = MAGENTA;
	const Color color = YELLOW;

	const short fontSize = 50;
	const short arrowsLenght = 65;

	struct Words
	{
		Vector2 pos;
	};

	Vector2 mouse;

	Words controls;
	Words paddleColor;
	Words gameMode;

	mouse.x = GetMouseX();
	mouse.y = GetMouseY();

	controls.pos.x = 100;
	controls.pos.y = 150;

	paddleColor.pos.x = 100;
	paddleColor.pos.y = 200;

	gameMode.pos.x = 100;
	gameMode.pos.y = 250;

	ClearBackground(PURPLE);
	DrawGameContourn(VIOLET);

	DrawText("Put your mouse over the characteristic you want to", 50, 50, 27, VIOLET);
	DrawText("change and use A and D or the arrows to select the", 50, 75, 27, VIOLET);
	DrawText("option you want to change.", 50, 100, 27, VIOLET);
	DrawText("Press enter to go back to menu", 150, 355, 30, VIOLET);

	switch (currentPlaceInOptions)
	{
	case PLACEINOPTIONS::CONTROLS:
		DrawText("-> Controls", controls.pos.x - arrowsLenght, controls.pos.y, fontSize, colorSelected);
		DrawText("Paddle color", paddleColor.pos.x, paddleColor.pos.y, fontSize, color);
		DrawText("Game mode", gameMode.pos.x, gameMode.pos.y, fontSize, color);
		break;
	case PLACEINOPTIONS::PADDLECOLOR:
		DrawText("Controls", controls.pos.x, controls.pos.y, fontSize, color);
		DrawText("-> Paddle color", paddleColor.pos.x - arrowsLenght, paddleColor.pos.y, fontSize, colorSelected);
		DrawText("Game mode", gameMode.pos.x, gameMode.pos.y, fontSize, color);
		break;
	case PLACEINOPTIONS::GAMEMODE:
		DrawText("Controls", controls.pos.x, controls.pos.y, fontSize, color);
		DrawText("Paddle color", paddleColor.pos.x, paddleColor.pos.y, fontSize, color);
		DrawText("-> Game mode", gameMode.pos.x - arrowsLenght, gameMode.pos.y, fontSize, colorSelected);
		break;
	case PLACEINOPTIONS::NONE:
		DrawText("Controls", controls.pos.x, controls.pos.y, fontSize, color);
		DrawText("Paddle color", paddleColor.pos.x, paddleColor.pos.y, fontSize, color);
		DrawText("Game mode", gameMode.pos.x, gameMode.pos.y, fontSize, color);
		break;
	default:
		break;
	}
}

void DrawChoicesInOptionsScreen(PLACEINOPTIONS &currentPlaceInOptions, Player &player1, Player &player2)
{
	const Color color = YELLOW;
	const short fontSize = 30;

	struct Options
	{
		Vector2 pos;
		short maxOptions;
	};

	Options controls;
	Options paddleColor;
	Options gameMode;

	controls.maxOptions = 4;
	controls.pos.x = 460;
	controls.pos.y = 165;
	//----------------------
	paddleColor.maxOptions = 22;
	paddleColor.pos.x = 460;
	paddleColor.pos.y = 190;
	//----------------------
	gameMode.maxOptions = 2;
	gameMode.pos.x = 420;
	gameMode.pos.y = 265;

	switch (currentPlaceInOptions)
	{
	case PLACEINOPTIONS::CONTROLS:
		if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
			SetUpMovementForConfigurations(controls.maxOptions);
		switch (configOption)
		{
		case 1:
			DrawText(FormatText("P1 up:  %c", player1.up), controls.pos.x, controls.pos.y, fontSize, color);

			if (GetKeyPressed() != KEY_P - 32 && GetKeyPressed() <= 122 && GetKeyPressed() >= 97) //&& GetKeyPressed() != player1.down - 32 && GetKeyPressed() != player2.up - 32 && GetKeyPressed() != player2.down - 32)
				player1.up = GetKeyPressed() - 32;
			break;
		case 2:
			DrawText(FormatText("P1 down:  %c", player1.down), controls.pos.x, controls.pos.y, fontSize, color);

			if (GetKeyPressed() != KEY_P - 32 && GetKeyPressed() <= 122 && GetKeyPressed() >= 97) //&& GetKeyPressed() != player1.up - 32 && GetKeyPressed() != player2.up - 32 && GetKeyPressed() != player2.down - 32)
				player1.down = GetKeyPressed() - 32;
			break;
		case 3:
			DrawText(FormatText("P2 up:  %c", player2.up), controls.pos.x, controls.pos.y, fontSize, color);

			if (GetKeyPressed() != KEY_P - 32  && GetKeyPressed() <= 122 && GetKeyPressed() >= 97 ) //&& GetKeyPressed() != player2.down - 32 && GetKeyPressed() != player1.up - 32 && GetKeyPressed() != player1.down - 32)
				player2.up = GetKeyPressed() - 32;
			break;
		case 4:
			DrawText(FormatText("P2 down:  %c", player2.down), controls.pos.x, controls.pos.y, fontSize, color);

			if (GetKeyPressed() != KEY_P - 32 && GetKeyPressed() <= 122 && GetKeyPressed() >= 97) //&& GetKeyPressed() != player2.up - 32 && GetKeyPressed() != player1.up - 32 && GetKeyPressed() != player1.down - 32)
				player2.down = GetKeyPressed() - 32;
			break;
		default:
			break;
		}
		player1.up = KeyW;
		player1.down = KeyS;
		player2.up = KEY_U;
		player2.down = KEY_J;
		break;
	case PLACEINOPTIONS::PADDLECOLOR:
		if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_D) || IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
		{
			SetUpMovementForColorOptions(paddleColor.maxOptions);
			SetPaddleP1ColorSelected(player1);
			SetPaddleP2ColorSelected(player2);
		}

		DrawText("P1(A - D):", paddleColor.pos.x, paddleColor.pos.y, fontSize, color);
		DrawText("P2(<- - ->):", paddleColor.pos.x, paddleColor.pos.y + fontSize, fontSize, color);
		DrawText("swatch", paddleColor.pos.x + 170, paddleColor.pos.y, fontSize, player1.color);
		DrawText("swatch", paddleColor.pos.x + 170, paddleColor.pos.y + fontSize, fontSize, player2.color);
		break;
	case PLACEINOPTIONS::GAMEMODE:
		if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_D) || IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
			SetUpMovementForGameMode(gameMode.maxOptions);
		switch (gameModeOption)
		{
		case 1:
			DrawText("Player vs Player  ", gameMode.pos.x, gameMode.pos.y, fontSize, color);
			PvsP = true;
			break;
		case 2:
			DrawText("Player vs Computer", gameMode.pos.x, gameMode.pos.y, fontSize, color);
			PvsP = false;
			break;
		default:
			break;
		}
		break;
	case PLACEINOPTIONS::NONE:
	default:
		break;
	}
}

void DrawPauseScreen()
{
	const Color colorPlaceInMenu = MAGENTA;
	const Color colorInMenu = YELLOW;

	const short fontSize = 100;
	const short arrowsLenght = 90;

	struct Word
	{
		Vector2 pos;
	};

	Word pause;

	pause.pos.x = 250;
	pause.pos.y = 175;

	ClearBackground(PURPLE);
	DrawGameContourn(VIOLET);
	DrawText("Pause", pause.pos.x, pause.pos.y, fontSize, colorInMenu);
	DrawText("Press enter to go back to menu", 190, 30, 26, VIOLET);
	DrawText("Press P to continue the game", 200, 394, 26, VIOLET);
}

void DrawPowerUp()
{
	switch (actualPowerUp)
	{
	case POWERUPS::SHIELD:
		if (powerUps[0].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[0].color);	break;
	case POWERUPS::BIGGERPADDLE:
		if (powerUps[1].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[1].color);	break;
	case POWERUPS::MORESPEED:
		if (powerUps[2].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[2].color);	break;
	case POWERUPS::MULTIBALL:			
		if (powerUps[3].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[3].color);	break;
	case POWERUPS::OBSTACLESINSCREEN:	
		if (powerUps[4].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[4].color);	break;
	case POWERUPS::INVERTSPEED:			
		if (powerUps[5].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[5].color);	break;
	case POWERUPS::INVERTCONTROLS:		
		if (powerUps[6].onScreen) DrawCircle(powerUpPos.x, powerUpPos.y, powerUps[0].radius, powerUps[6].color);	break;
	default:	break;
	}
}

void Initialization()
{
	menu::initialization();
	match::initialization();
	options::initialization();
}

void Input()
{
	switch (currentPlaceInGame)
	{
	case PLACEINGAME::MENU:
		menu::input();
		break;
	case PLACEINGAME::MATCH:
		match::input();
		break;
	case PLACEINGAME::OPTIONS:
		options::input();
		break;
	case PLACEINGAME::RULES:
		rules::input();
		break;
	default:
		break;
	}
}

void Update()
{
	switch (currentPlaceInGame)
	{
	case PLACEINGAME::MENU:
		menu::update();
		break;
	case PLACEINGAME::MATCH:
		match::update();
		break;
	case PLACEINGAME::EXIT:
		CloseWindow();
	default:
		break;
	}
}

void Draw()
{
	BeginDrawing();
	switch (currentPlaceInGame)
	{
	case PLACEINGAME::MENU:
		menu::draw();
		break;
	case PLACEINGAME::MATCH:
		match::draw();
		break;
	case PLACEINGAME::OPTIONS:
		options::draw();
		break;
	case PLACEINGAME::RULES:
		rules::draw();
		break;
	default:
		break;
	}
	EndDrawing();
}