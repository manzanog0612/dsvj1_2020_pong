#pragma once
#include "game/game.h"

#include "elements/elements_library.h"
#include "functions library/movement_library.h"

void PlayerMovementDefaultDefinition(Player &player1, Player &player2);

void FirstElementsDefinitions(Player &player1, Player &player2, Ball &ball);

void SetPlayersMovement(Player &player1, Player &player2, Ball ball);

void RandomizeBallDirection(Ball &ball);

void SetBallMovement(Rectangle rec1, Rectangle rec2, Ball &ball);

void SetLastBallTouch(Player &player1, Player &player2, Ball &ball);

bool CheckIfBallOnGame(Ball &ball);

void ResetGame(Player &player1, Player &player2, Ball &ball);

void AddPointToPlayer(Player &player1, Player &player2, Ball &ball);

void SetScore(Player &player1, Player &player2, Ball &ball);

void RestartScore(Player &player1, Player &player2);

void SelectMenuChoiceToHightlight(PLACEINGAME &futurePlaceInGame);

void SelectOptionsChoiceToHightlight(PLACEINOPTIONS &currentPlaceInOptions);

bool CheckIfMatchEnded(Player player1, Player player2);

void AddMatchPointToPlayer(Player &player1, Player &player2);

void GoToMenu(PLACEINGAME &currentPlaceInGame);

void GoToPlaceSelected(PLACEINGAME &currentPlaceInGame, PLACEINGAME &futurePlaceInGame);

void SetUpMovementForConfigurations(short maxOption);

//------------------------------------------------------------------------------
// PowerUps

void PowerUpsInitialization();

double TimeForNextPowerUp();

POWERUPS PowerUpSelected();

Vector2 RandomicePowerUpPosition();

void ActivateViewPowerUp();

bool VerifyAllPowerUpsOff();

bool VerifyPowerUpCollision(Ball ball);

void SetOffPowerUpPosition();

void SetUpPowerUp(Player player1);

void ActivatePowerUp(Player &player1, Player &player2, Ball &ball, Ball balls[amountMultiBalls]);

void DrawPowerUp();

void SetUpShield(Player player1, Player player2, Ball &ball);

void SetUpBiggerPaddle(Player &player1, Player &player2);

void SetUpMoreSpeed(Player &player1, Player &player2);

void MultiBallDeclarations(Ball balls[amountMultiBalls]);

void AddPointToPlayerMultiBall(Player &player1, Player &player2, Ball &ball, Ball balls[amountMultiBalls]);

void SetMultiBallMovement(Player &player1, Player &player2, Ball balls[amountMultiBalls]);

bool CheckIfBallsOnGame(Ball balls[amountMultiBalls]);

void DrawMultiBalls(Ball balls[amountMultiBalls]);

void AddPointToPlayerMultiBall(Player &player1, Player &player2, Ball &ball, Ball balls[amountMultiBalls]);

void ObstaclesDeclaration();

void DrawObstacles();

void SetBallMovementForObstacle(Rectangle rec1, Rectangle rec2, Ball &ball);

void InvertControls(Player &player1, Player &player2);

void DeactivatePowerUps(Player &player1, Player &player2);

void DeactivatePowerUpsByTime(Player &player1, Player &player2);

void SetTimeForPowerUp();

//------------------------------------------------------------------------------
// To draw

void DrawGameContourn(Color colorChoice);

void DrawPlayerPoints(Player player1, Player player2);

void DrawMatchPoint(Player player1, Player player2);

void DrawElements(Player &player1, Player &player2, Ball &ball);

void DrawGameSpace(Player &player1, Player &player2, Ball &ball);

void DrawMenu(PLACEINGAME &futurePlaceInGame);

void DrawWinnerScreen(Player &player1, Player &player2);

void DrawRules(Player &player1, Player &player2);

void DrawOptions(PLACEINOPTIONS &futurePlaceInOptions);

void DrawChoicesInOptionsScreen(PLACEINOPTIONS &futurePlaceInOptions, Player &player1, Player &player2);

void DrawPauseScreen();

//------------------------------------------------------------------------------

void Initialization();

void Input();

void Update();

void Draw();