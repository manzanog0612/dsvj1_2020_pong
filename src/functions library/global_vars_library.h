#ifndef GLOBAL_VARS_LIBRARY_H
#define GLOBAL_VARS_LIBRARY_H

#include "elements/elements_properties.h"
#include "functions library/movement_library.h"

using namespace elements_properties;
using namespace movement_library;

namespace global_vars
{
	extern Player player1;
	extern Player player2;
	extern Ball ball;
	extern Ball balls[amountMultiBalls];
	extern PLACEINGAME currentPlaceInGame;
	extern PLACEINGAME futurePlaceInGame;
	extern PLACEINOPTIONS  currentPlaceInOptions;

	extern bool inGame;
	extern bool pauseMatch;
	extern bool player1LastTouch;
	extern double timeInGame;

	extern	bool PvsP;
	extern bool invertSpeed;
	extern bool invertControls;
	extern bool powerUpTimeDurationSet;
	extern bool activateMultiBallDeclaration;
	extern double powerUpTimeDuration;
	extern double powerUpEndingTime;

	extern POWERUPS actualPowerUp;
	extern PowerUps powerUps[static_cast<int>(amountPowerUps)];
	extern Vector2 powerUpPos;
	extern Rectangle obstacles[static_cast<int>(amountObtacles)];

	extern short configOption;
	extern short paddleColorOptionP1;
	extern short paddleColorOptionP2;
	extern short ballColorOption;
	extern short gameModeOption;

	extern float ballModifier;

	extern bool goToScreenLimits;
	extern bool obstaclesDeclarated;

	extern bool playingGame;
}

#endif //GLOBAL_VARS_LIBRARY_H