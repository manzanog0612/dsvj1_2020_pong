#include <iostream>

#include "raylib.h"

#include "game/game.h"

int main(void)
{
	game::run();
	CloseWindow();
	return 0;
}