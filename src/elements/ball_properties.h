#ifndef BALL_PROPERTIES_H
#define BALL_PROPERTIES_H

namespace ball_properties
{
	struct CenterBall
	{
		bool addToX;
		bool addToY;
	};

	enum class STATEOFBALL { ONCOLISION, HAVECOLISIONED, NOTONCOLISION };

	struct State
	{
		STATEOFBALL actual;
		STATEOFBALL previous;
	};
}
#endif //BALL_PROPERTIES_H