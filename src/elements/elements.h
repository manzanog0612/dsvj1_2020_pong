#ifndef ELEMENTS_H
#define ELEMENTS_H

#include "raylib.h"

namespace elements
{
	void firstElementsDefinitions();

	void drawElements();

	Color getColorSelected(short colorOption);

	namespace ball_logic
	{
		void randomizeBallDirection();

		void setBallMovement();

		void setLastBallTouch();

		bool checkIfBallOnGame();

		void setBallColorSelected();
	}

	namespace players
	{
		void playerMovementDefaultDefinition();

		void setPlayersMovement();

		void addPointToPlayer();

		void addMatchPointToPlayer();

		void setPaddleP1ColorSelected();

		void setPaddleP2ColorSelected();
	}
}

#endif //ELEMENTS_H