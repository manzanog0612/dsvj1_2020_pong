#include "elements/elements.h"

#include "raylib.h"

#include "functions library/global_vars_library.h"
#include "functions library/powerups_library.h"

using namespace global_vars;
using namespace powerup;
using namespace elements_properties;
using namespace movement_library;

namespace elements
{
	void firstElementsDefinitions()
	{
		player1.rec.height = 100;
		player1.rec.width = 26;
		player1.rec.x = static_cast<float>(screenLimit.left + contourLineThickness * 2 + 7);
		player1.rec.y = static_cast<float>(screenHeight / 2 - player1.rec.height / 2);
		player1.speed = 2;

		player2.rec.height = 100;
		player2.rec.width = 26;
		player2.rec.x = static_cast<float>(screenLimit.right - contourLineThickness * 2 - 7 - player2.rec.width);
		player2.rec.y = static_cast<float>(screenHeight / 2 - player2.rec.height / 2);
		player2.speed = 2;

		ball.center.x = static_cast<float>(screenWidth / 2);
		ball.center.y = static_cast<float>(screenHeight / 2);
		ball.state.actual = STATEOFBALL::NOTONCOLISION;
		ball.state.previous = STATEOFBALL::NOTONCOLISION;
	}

	void drawElements()
	{
		DrawRectangleRec(player1.rec, player1.color);
		DrawRectangleRec(player2.rec, player2.color);
		DrawCircleV(ball.center, ball.radius, ball.color);
	}

	Color getColorSelected(short colorOption)
	{
		COLOR color = static_cast<COLOR>(colorOption);

		switch (color)
		{
		case COLOR::_GRAY:		return GRAY;
		case COLOR::_DARKGRAY:	return DARKGRAY;
		case COLOR::_YELLOW:	return YELLOW;
		case COLOR::_GOLD:		return GOLD;
		case COLOR::_ORANGE:	return ORANGE;
		case COLOR::_PINK:		return PINK;
		case COLOR::_RED:		return RED;
		case COLOR::_MAROON:	return MAROON;
		case COLOR::_GREEN:		return GREEN;
		case COLOR::_DARKGREEN:	return DARKGREEN;
		case COLOR::_SKYBLUE:	return SKYBLUE;
		case COLOR::_BLUE:		return BLUE;
		case COLOR::_DARKBLUE:	return DARKBLUE;
		case COLOR::_VIOLET:	return VIOLET;
		case COLOR::_DARKPURPLE:return DARKPURPLE;
		case COLOR::_BEIGE:		return BEIGE;
		case COLOR::_BROWN:		return BROWN;
		case COLOR::_DARKBROWN:	return DARKBROWN;
		case COLOR::_WHITE:		return WHITE;
		case COLOR::_BLACK:		return BLACK;
		case COLOR::_MAGENTA:	return MAGENTA;
		case COLOR::_RAYWHITE:	return RAYWHITE;
		default:				return VIOLET;
		}
	}

	namespace ball_logic
	{
		void randomizeBallDirection()
		{
			ball.modifyCenter.addToX = GetRandomValue(1, 2) == 1;
			ball.modifyCenter.addToY = GetRandomValue(1, 2) == 1;
		}

		void setBallMovement()
		{
			float auxBallCenterX;
			float auxBallRadius;

			if (ball.center.x - ball.radius > player1.rec.x + player1.rec.width &&
				ball.center.x + ball.radius < player2.rec.x)
				ball.state.actual = STATEOFBALL::NOTONCOLISION;

			if ((CheckCollisionCircleRec(ball.center, ball.radius, player1.rec) ||
				CheckCollisionCircleRec(ball.center, ball.radius, player2.rec)) ||
				(powerUps[static_cast<short>(POWERUPS::OBSTACLESINSCREEN)].on) &&
				((CheckCollisionCircleRec(ball.center, ball.radius, obstacles[static_cast<short>(OBSTACLES::UP)]) ||
					CheckCollisionCircleRec(ball.center, ball.radius, obstacles[static_cast<short>(OBSTACLES::DOWN)]))))
				ball.state.actual = STATEOFBALL::ONCOLISION;

			if (ball.state.previous == STATEOFBALL::ONCOLISION && ball.state.actual == STATEOFBALL::ONCOLISION)
				ball.state.actual = STATEOFBALL::HAVECOLISIONED;
			else
				ball.state.previous = ball.state.actual;

			switch (ball.state.actual)
			{
			case STATEOFBALL::ONCOLISION:
				auxBallCenterX = ball.center.x;
				auxBallRadius = ball.radius;

				if (CheckCollisionCircleRec(ball.center, ball.radius, player1.rec))
				{
					if (ball.center.x - ball.radius / 2 < player1.rec.x + player1.rec.width)
					{
						ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
						if (invertSpeed)
							ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
					}
					else
					{
						ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
						if (invertSpeed)
							ball.modifyCenter.addToY = !ball.modifyCenter.addToY;

						if (ball.center.y > (player1.rec.y + player1.rec.height / 2))
							ballModifier = ball.center.y / (player1.rec.y + player1.rec.height / 2);
						else
							ballModifier = (player1.rec.y + player1.rec.height / 2) / ball.center.y;
					}
					ball.speed.x += 0.1f;
				}
				else if (CheckCollisionCircleRec(ball.center, ball.radius, player2.rec))
				{
					if (ball.center.x + ball.radius / 2 > player2.rec.x)
					{
						ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
						if (invertSpeed)
							ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
					}
					else
					{
						ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
						if (invertSpeed)
							ball.modifyCenter.addToY = !ball.modifyCenter.addToY;

						if (ball.center.y > (player2.rec.y + player2.rec.height / 2))
							ballModifier = ball.center.y / (player2.rec.y + player2.rec.height / 2);
						else
							ballModifier = (player2.rec.y + player2.rec.height / 2) / ball.center.y;
					}
					ball.speed.x += 0.1f;
					if (ball.speed.x>= 3.0)
						ball.speed.y += 0.1f;
				}
				if ((powerUps[static_cast<short>(POWERUPS::OBSTACLESINSCREEN)].on))
				{
					if (CheckCollisionCircleRec(ball.center, ball.radius, obstacles[static_cast<short>(OBSTACLES::UP)]) ||
						CheckCollisionCircleRec(ball.center, ball.radius, obstacles[static_cast<short>(OBSTACLES::DOWN)]))
					{
						if (ball.center.x > obstacles[static_cast<short>(OBSTACLES::UP)].x + obstacles[static_cast<short>(OBSTACLES::UP)].width)
						{
							ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
							if (invertSpeed)
								ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
						}
						else if (ball.center.x < obstacles[static_cast<short>(OBSTACLES::UP)].x)
						{
							ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
							if (invertSpeed)
								ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
						}
						else
						{
							ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
							if (invertSpeed)
								ball.modifyCenter.addToX = !ball.modifyCenter.addToX;
						}
					}
				}
				break;
			case STATEOFBALL::HAVECOLISIONED:
			case STATEOFBALL::NOTONCOLISION:
				if (ball.center.y - ball.radius <= screenLimit.up + contourLineThickness ||
					ball.center.y + ball.radius >= screenLimit.down - contourLineThickness)
				{
					ball.modifyCenter.addToY = !ball.modifyCenter.addToY;
				}
				if (ball.modifyCenter.addToY) ball.center.y += ball.speed.y* ballModifier;
				else ball.center.y -= ball.speed.y * ballModifier;
				if (ball.modifyCenter.addToX) ball.center.x += ball.speed.x;
				else ball.center.x -= ball.speed.x;
				break;
			default:
				break;
			}
		}

		void setLastBallTouch()
		{
			if (CheckCollisionCircleRec(ball.center, ball.radius, player1.rec))
			{
				player1.lastTouch = true;
				player2.lastTouch = false;
			}
			else if (CheckCollisionCircleRec(ball.center, ball.radius, player2.rec))
			{
				player2.lastTouch = true;
				player1.lastTouch = false;
			}
		}

		bool checkIfBallOnGame()
		{
			return (ball.center.x - ball.radius > screenLimit.left + contourLineThickness &&
				ball.center.x + ball.radius < screenLimit.right - contourLineThickness);
		}

		void setBallColorSelected()
		{
			ball.color = elements::getColorSelected(ballColorOption);
		}
	}

	namespace players
	{
		void playerMovementDefaultDefinition()
		{
			player1.up = KeyW;
			player1.down = KeyS;
			player2.up = KEY_U;
			player2.down = KEY_J;
			player1.color = RED;
			player2.color = BLUE;
		}

		void setPlayersMovement()
		{
			float auxPlayer1 = player1.rec.y;
			float auxPlayer2 = player2.rec.y;

			if (IsKeyPressed(player1.up) || IsKeyDown(player1.up))  auxPlayer1 -= player1.speed;
			if (IsKeyPressed(player1.down) || IsKeyDown(player1.down)) auxPlayer1 += player1.speed;

			if (PvsP)
			{
				if (IsKeyPressed(player2.up) || IsKeyDown(player2.up)) auxPlayer2 -= player2.speed;
				if (IsKeyPressed(player2.down) || IsKeyDown(player2.down))  auxPlayer2 += player2.speed;
			}
			else
			{
				if (ball.center.y < player2.rec.y + player2.rec.height / 4) auxPlayer2 -= player2.speed;
				if (ball.center.y > player2.rec.y + player2.rec.height / 4) auxPlayer2 += player2.speed;
			}
			// Limits the movement of the rectangles to the screen
			if (auxPlayer1 >= screenLimit.up + contourLineThickness &&
				auxPlayer1 <= screenLimit.down - contourLineThickness - player1.rec.height)
				player1.rec.y = auxPlayer1;
			if (auxPlayer2 >= screenLimit.up + contourLineThickness &&
				auxPlayer2 <= screenLimit.down - contourLineThickness - player2.rec.height)
				player2.rec.y = auxPlayer2;
		}

		void addPointToPlayer()
		{
			if (ball.center.x + ball.radius >= screenLimit.right - contourLineThickness)
				player1.points++;
			else player2.points++;
		}

		void addMatchPointToPlayer()
		{
			if (player1.points == player1.maxPoints)
				player1.roundsWon++;
			if (player2.points == player2.maxPoints)
				player2.roundsWon++;
		}

		void setPaddleP1ColorSelected()
		{
			player1.color = elements::getColorSelected(paddleColorOptionP1);
		}

		void setPaddleP2ColorSelected()
		{
			player2.color = elements::getColorSelected(paddleColorOptionP2);
		}
	}
}