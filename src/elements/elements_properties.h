#ifndef ELEMENTS_PROPERTIES_H
#define ELEMENTS_PROPERTIES_H

#include "raylib.h"

#include "ball_properties.h"

using namespace ball_properties;

namespace elements_properties
{
	enum class COLOR {
		_GRAY = 1, _DARKGRAY, _YELLOW, _GOLD, _ORANGE, _PINK, _RED, _MAROON, _GREEN, _DARKGREEN, _SKYBLUE,
		_BLUE, _DARKBLUE, _VIOLET, _DARKPURPLE, _BEIGE, _BROWN, _DARKBROWN, _WHITE, _BLACK, _MAGENTA, _RAYWHITE
	};

	enum class POWERUPS { SHIELD, BIGGERPADDLE, MORESPEED, MULTIBALL, OBSTACLESINSCREEN, INVERTSPEED, INVERTCONTROLS };

	enum class POWERUPSPOSITIONS { RIGHT_UP, CENTRE_UP, LEFT_UP, RIGHT_DOWN, CENTRE_DOWN, LEFT_DOWN };

	enum class OBSTACLES { UP, DOWN };

	const POWERUPS amountPowerUps = static_cast<POWERUPS>(7);
	const short amountMultiBalls = 2;
	const OBSTACLES amountObtacles = static_cast<OBSTACLES>(2);
	const POWERUPSPOSITIONS amountPowerUpsPositions = static_cast<POWERUPSPOSITIONS>(6);

	struct Player
	{
		Rectangle rec;
		Color color;
		short points = 0;
		short roundsWon = 0;
		short speed;
		int up;
		int down;
		bool lastTouch;
		const short maxPoints = 10;
	};

	struct Ball
	{
		Vector2 center;
		const float radius = 13;
		Color color = YELLOW;
		CenterBall modifyCenter;
		State state;
		Vector2 speed;
	};

	struct PowerUps
	{
		Vector2 pos[static_cast<int>(amountPowerUpsPositions)];
		const float radius = 20;
		bool onScreen = false;
		bool on = false;
		Color color;
		short time;
	};
}

#endif