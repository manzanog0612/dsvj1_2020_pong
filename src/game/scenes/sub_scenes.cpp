#include "game/scenes/sub_scenes.h"

#include "functions library/global_vars_library.h"
#include "functions library/global_drawing_vars_library.h"
#include "elements/elements.h"
#include "functions library/powerups_library.h"

using namespace global_vars;
using namespace global_drawing_vars;
using namespace elements;
using namespace powerup;
using namespace elements_properties;
using namespace movement_library;

namespace sub_scenes
{
	namespace score
	{
		void resetGame()
		{
			ballModifier = 1;
			ball.speed.x = 2;
			ball.speed.y = 2;
			powerups_logic::deactivatePowerUps();
			powerups_logic::setTimeForPowerUp();
			elements::firstElementsDefinitions();
			elements::ball_logic::randomizeBallDirection();
		}

		void setScore()
		{
			if (!elements::ball_logic::checkIfBallOnGame())
			{
				elements::players::addPointToPlayer();
				score::resetGame();
			}
		}

		void restartScore()
		{
			player1.points = 0;
			player2.points = 0;
		}

		bool checkIfMatchEnded()
		{
			const short maxPlayerPoints = 10;
			return (player1.points == maxPlayerPoints || player2.points == maxPlayerPoints);
		}

		void drawPlayerPoints()
		{
			const short fontSize = 100;
			const short posY = 180;
			const short posXPlayer1 = 296;
			const short posXPlayer2 = 452;
			const Color color = VIOLET;
			const short posX = 380;

			hyphen.text = "-";

			DrawText(FormatText("%01i", player1.points), ((screenWidth / 2 - MeasureText(FormatText("%01i", player1.points), fontSize) / 2) -
				MeasureText(hyphen.text, fontSize)*2), posY, fontSize, color);

			DrawText(hyphen.text, (screenWidth / 2 - MeasureText(hyphen.text, fontSize) / 2), posY, fontSize, color);

			DrawText(FormatText("%01i", player2.points), ((screenWidth / 2 - MeasureText(FormatText("%01i", player2.points), fontSize) / 2) + MeasureText(hyphen.text, fontSize) * 2) , posY, fontSize, color);
		}

		void drawMatchPoint()
		{
			Color color = YELLOW;
			short fontSize = 17;
			short p1posX = 105;
			short p2posX = 610;
			short p1NumberposX = 143;
			short p2NumberposX = 648;

			p1RoundsWon.text = "Rounds Won";
			p2RoundsWon.text = "Rounds Won";

			p1RoundsWon.posY = 380;
			p2RoundsWon.posY = 380;

			p1RoundsWonNumber.posY = p1RoundsWon.posY + fontSize;
			p2RoundsWonNumber.posY = p2RoundsWon.posY + fontSize;

			p1RoundsWonNumber.fontSize = fontSize + 13;
			p2RoundsWonNumber.fontSize = fontSize + 13;

			DrawText(p1RoundsWon.text, static_cast<int>((player1.rec.x + player1.rec.width*2)), static_cast<int>(p1RoundsWon.posY), fontSize, color);
			DrawText(p2RoundsWon.text, static_cast<int>((player2.rec.x - player2.rec.width - MeasureText(p2RoundsWon.text, fontSize))), static_cast<int>(p1RoundsWon.posY), fontSize, color);

			DrawText(FormatText("%01i", player1.roundsWon), static_cast<int>(((player1.rec.x + player1.rec.width * 2) + (MeasureText(p1RoundsWon.text, fontSize)/2) -
				(MeasureText(FormatText("%01i", player1.roundsWon), fontSize) / 2))), static_cast<int>(p1RoundsWonNumber.posY), p1RoundsWonNumber.fontSize, color);
			DrawText(FormatText("%01i", player2.roundsWon), static_cast<int>(((player2.rec.x - player2.rec.width - MeasureText(p2RoundsWon.text, fontSize)) + (MeasureText(p2RoundsWon.text, fontSize) / 2) -
				(MeasureText(FormatText("%01i", player2.roundsWon), fontSize) / 2))), static_cast<int>(p2RoundsWonNumber.posY), p2RoundsWonNumber.fontSize, color);
		}
	}

	namespace movement_through_game
	{
		void selectMenuChoiceToHightlight()
		{
			Vector2 mouse;

			mouse.x = static_cast<float>(GetMouseX());
			mouse.y = static_cast<float>(GetMouseY());

			if (mouse.y >= play.posY && mouse.y <= play.posY + play.fontSize &&
				mouse.x >= ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)) && 
				mouse.x <= ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)) + MeasureText(play.text, play.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::MATCH;
			}
			else if (mouse.y >= options.posY && mouse.y <= options.posY + options.fontSize &&
				mouse.x >= ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)) &&
				mouse.x <= ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)) + MeasureText(options.text, options.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::OPTIONS;
			}
			else if (mouse.y >= rules.posY && mouse.y <= rules.posY + rules.fontSize &&
				mouse.x >= ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)) &&
				mouse.x <= ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)) + MeasureText(rules.text, rules.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::RULES;
			}
			else if (mouse.y >= credits.posY && mouse.y <= credits.posY + credits.fontSize &&
				mouse.x >= ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)) &&
				mouse.x <= ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)) + MeasureText(credits.text, credits.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::CREDITS;
			}
			else if (mouse.y >= exit.posY && mouse.y <= exit.posY + exit.fontSize &&
				mouse.x >= ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)) &&
				mouse.x <= ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)) + MeasureText(exit.text, exit.fontSize))
			{
				futurePlaceInGame = PLACEINGAME::EXIT;
			}
			else
			{
				futurePlaceInGame = PLACEINGAME::NONE;
			}
		}

		void selectOptionsChoiceToHightlight()
		{
			short posX = 100;

			Vector2 mouse;

			mouse.x = static_cast<float>(GetMouseX());
			mouse.y = static_cast<float>(GetMouseY());

			if (mouse.y >= controls.posY && mouse.y <= controls.posY + controls.fontSize &&
				mouse.x >= posX && mouse.x <= posX + MeasureText(controls.text, controls.fontSize))
			{
				currentPlaceInOptions = PLACEINOPTIONS::CONTROLS;
			}
			else if (mouse.y >= paddleColor.posY && mouse.y <= paddleColor.posY + paddleColor.fontSize &&
				mouse.x >= posX && mouse.x <= posX + MeasureText(paddleColor.text, paddleColor.fontSize))
			{
				currentPlaceInOptions = PLACEINOPTIONS::PADDLECOLOR;
			}
			else if (mouse.y >= ballColor.posY && mouse.y <= ballColor.posY + ballColor.fontSize &&
				mouse.x >= posX && mouse.x <= posX + MeasureText(ballColor.text, ballColor.fontSize))
			{
				currentPlaceInOptions = PLACEINOPTIONS::BALLCOLOR;
			}
			else if (mouse.y >= gameMode.posY && mouse.y <= gameMode.posY + gameMode.fontSize &&
				mouse.x >= posX && mouse.x <= posX + MeasureText(gameMode.text, gameMode.fontSize))
			{
				currentPlaceInOptions = PLACEINOPTIONS::GAMEMODE;
			}
			else
			{
				currentPlaceInOptions = PLACEINOPTIONS::NONE;
			}
		}

		void goToMenu()
		{
			currentPlaceInGame = PLACEINGAME::MENU;
		}

		void goToPlaceSelected()
		{
			currentPlaceInGame = futurePlaceInGame;
		}

		void setUpMovementForGameMode(short maxOption)
		{
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
			{
				gameModeOption -= 1;
				if (gameModeOption < 1) gameModeOption = maxOption;
			}
			if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
			{
				gameModeOption += 1;
				if (gameModeOption > maxOption) gameModeOption = 1;
			}
		}

		void setUpMovementForConfigurations(short maxOption)
		{
			if (IsKeyPressed(KEY_LEFT))
			{
				configOption -= 1;
				if (configOption < 1) configOption = maxOption;
			}
			if (IsKeyPressed(KEY_RIGHT))
			{
				configOption += 1;
				if (configOption > maxOption) configOption = 1;
			}
		}

		void setUpMovementForColorOptions(short maxOption)
		{
			if (IsKeyPressed(KEY_A))
			{
				paddleColorOptionP1 -= 1;
				if (paddleColorOptionP1 < 1) paddleColorOptionP1 = maxOption;
			}
			if (IsKeyPressed(KEY_D))
			{
				paddleColorOptionP1 += 1;
				if (paddleColorOptionP1 > maxOption) paddleColorOptionP1 = 1;
			}

			if (IsKeyPressed(KEY_LEFT))
			{
				paddleColorOptionP2 -= 1;
				if (paddleColorOptionP2 < 1) paddleColorOptionP2 = maxOption;
			}
			if (IsKeyPressed(KEY_RIGHT))
			{
				paddleColorOptionP2 += 1;
				if (paddleColorOptionP2 > maxOption) paddleColorOptionP2 = 1;
			}
		}

		void setUpMovementForBallColorOptions(short maxOption)
		{
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
			{
				ballColorOption -= 1;
				if (ballColorOption < 1) ballColorOption = maxOption;
			}
			if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
			{
				ballColorOption += 1;
				if (ballColorOption > maxOption) ballColorOption = 1;
			}
		}

		bool checkIfAnyKeyIsPressed()
		{
			return (IsKeyPressed(KEY_Q) || IsKeyPressed(KEY_W) || IsKeyPressed(KEY_E) || IsKeyPressed(KEY_R) || IsKeyPressed(KEY_T) ||
				IsKeyPressed(KEY_Y) || IsKeyPressed(KEY_U) || IsKeyPressed(KEY_I) || IsKeyPressed(KEY_O) || IsKeyPressed(KEY_M)	||
				IsKeyPressed(KEY_A) || IsKeyPressed(KEY_S) || IsKeyPressed(KEY_D) || IsKeyPressed(KEY_F) || IsKeyPressed(KEY_G) ||
				IsKeyPressed(KEY_H) || IsKeyPressed(KEY_J) || IsKeyPressed(KEY_K) || IsKeyPressed(KEY_L) || IsKeyPressed(KEY_Z) ||
				IsKeyPressed(KEY_X) || IsKeyPressed(KEY_C) || IsKeyPressed(KEY_V) || IsKeyPressed(KEY_B) || IsKeyPressed(KEY_N));
		}
	}
}
//--------------------------------------------------------
	/*void Initialization()
	{
		using namespace game;

		menu::menuInitialization();
		match::matchInitialization();
		options::optionsInitialization();
	}

	void input()
	{
		using namespace game;

		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:
			menu::menuInput();
			break;
		case PLACEINGAME::MATCH:
			match::matchInput();
			break;
		case PLACEINGAME::OPTIONS:
			options::optionsInput();
			break;
		case PLACEINGAME::RULES:
			rules::rulesInput();
			break;
		default:
			break;
		}
	}

	void update()
	{
		using namespace game;

		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:
			menu::menuUpdate();
			break;
		case PLACEINGAME::MATCH:
			match::matchUpdate();
			break;
		case PLACEINGAME::EXIT:
			CloseWindow();
		default:
			break;
		}
	}

	void drawAll()
	{
		using namespace game;

		BeginDrawing();
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:
			menu::menuDraw();
			break;
		case PLACEINGAME::MATCH:
			match::matchDraw();
			break;
		case PLACEINGAME::OPTIONS:
			options::optionsDraw();
			break;
		case PLACEINGAME::RULES:
			rules::rulesDraw();
			break;
		default:
			break;
		}
		EndDrawing();
	}
}*/
