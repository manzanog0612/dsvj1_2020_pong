#ifndef SCENES_DRAW_H
# define SCENES_DRAW_H

#include "functions library/global_vars_library.h"

namespace scenes_draw
{
	void drawMenu();

	void drawGameContourn(Color colorChoice);

	void drawGameSpace();

	void drawWinnerScreen();

	void drawPauseScreen();

	void drawRules();

	void drawOptions();

	void drawChoicesInOptionsScreen();

	void drawCredits();
}

#endif //SCENES_DRAW_H