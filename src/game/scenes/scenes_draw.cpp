#include "game/scenes/scenes_draw.h"

#include "functions library/global_vars_library.h"
#include "functions library/global_drawing_vars_library.h"
#include "game/scenes/sub_scenes.h"
#include "elements/elements.h"
 
using namespace global_vars;
using namespace global_drawing_vars;
using namespace sub_scenes;
using namespace elements;
using namespace movement_library;

namespace scenes_draw
{
	void drawMenu()
	{
		const Color colorPlaceInMenu = MAGENTA;
		const Color colorInMenu = YELLOW;

		const short arrowsLenght = 90;

		play.text = "Play";
		options.text = "Options";
		rules.text = "Rules";
		credits.text = "Credits";
		exit.text = "Exit";

		playAndArrows.text = "--> Play <--";
		optionsAndArrows.text = "--> Options <--";
		rulesAndArrows.text = "--> Rules <--";
		creditsAndArrows.text = "--> Credits <--";
		exitAndArrows.text = "--> Exit <--";

		gameVersion.text = "v1.0";

		play.posY = 170;
		options.posY = 215;
		rules.posY = 260;
		credits.posY = 305;
		exit.posY = 350;

		gameVersion.posY = screenLimit.down - contourLineThickness*2 - 5;

		play.fontSize = 45;
		options.fontSize = 45;
		rules.fontSize = 45;
		credits.fontSize = 45;
		exit.fontSize = 45;

		gameVersion.fontSize = 30;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);
		DrawText(title.text, ((screenWidth/2 - MeasureText(title.text, title.fontSize)/2)), title.posY, title.fontSize, colorInMenu);
		DrawText(gameVersion.text, (screenLimit.right - contourLineThickness*3/2 - MeasureText(gameVersion.text, gameVersion.fontSize)), gameVersion.posY, gameVersion.fontSize, YELLOW);

		switch (futurePlaceInGame)
		{
		case PLACEINGAME::MATCH:
			DrawText(playAndArrows.text, ((screenWidth / 2 - MeasureText(playAndArrows.text, play.fontSize) / 2)), play.posY, play.fontSize, colorPlaceInMenu);
			DrawText(options.text, ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)), options.posY, options.fontSize, colorInMenu);
			DrawText(rules.text, ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)), rules.posY, rules.fontSize, colorInMenu);
			DrawText(credits.text, ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)), credits.posY, credits.fontSize, colorInMenu);
			DrawText(exit.text, ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)), exit.posY, exit.fontSize, colorInMenu);
			break;
		case PLACEINGAME::OPTIONS:
			DrawText(play.text, ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)), play.posY, play.fontSize, colorInMenu);
			DrawText(optionsAndArrows.text, ((screenWidth / 2 - MeasureText(optionsAndArrows.text, play.fontSize) / 2)), options.posY, options.fontSize, colorPlaceInMenu);
			DrawText(rules.text, ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)), rules.posY, rules.fontSize, colorInMenu);
			DrawText(credits.text, ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)), credits.posY, credits.fontSize, colorInMenu);
			DrawText(exit.text, ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)), exit.posY, exit.fontSize, colorInMenu);
			break;
		case PLACEINGAME::RULES:
			DrawText(play.text, ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)), play.posY, play.fontSize, colorInMenu);
			DrawText(options.text, ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)), options.posY, options.fontSize, colorInMenu);
			DrawText(rulesAndArrows.text, ((screenWidth / 2 - MeasureText(rulesAndArrows.text, rules.fontSize) / 2)), rules.posY, rules.fontSize, colorPlaceInMenu);
			DrawText(credits.text, ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)), credits.posY, credits.fontSize, colorInMenu);
			DrawText(exit.text, ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)), exit.posY, exit.fontSize, colorInMenu);
			break;
		case PLACEINGAME::CREDITS:
			DrawText(play.text, ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)), play.posY, play.fontSize, colorInMenu);
			DrawText(options.text, ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)), options.posY, options.fontSize, colorInMenu);
			DrawText(rules.text, ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)), rules.posY, rules.fontSize, colorInMenu);
			DrawText(creditsAndArrows.text, ((screenWidth / 2 - MeasureText(creditsAndArrows.text, credits.fontSize) / 2)), credits.posY, credits.fontSize, colorPlaceInMenu);
			DrawText(exit.text, ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)), exit.posY, exit.fontSize, colorInMenu);
			break;
		case PLACEINGAME::EXIT:
			DrawText(play.text, ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)), play.posY, play.fontSize, colorInMenu);
			DrawText(options.text, ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)), options.posY, options.fontSize, colorInMenu);
			DrawText(rules.text, ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)), rules.posY, rules.fontSize, colorInMenu);
			DrawText(credits.text, ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)), credits.posY, credits.fontSize, colorInMenu);
			DrawText(exitAndArrows.text, ((screenWidth / 2 - MeasureText(exitAndArrows.text, exit.fontSize) / 2)), exit.posY, exit.fontSize, colorPlaceInMenu);
			break;
		case PLACEINGAME::NONE:
		default:
			DrawText(play.text, ((screenWidth / 2 - MeasureText(play.text, play.fontSize) / 2)), play.posY, play.fontSize, colorInMenu);
			DrawText(options.text, ((screenWidth / 2 - MeasureText(options.text, options.fontSize) / 2)), options.posY, options.fontSize, colorInMenu);
			DrawText(rules.text, ((screenWidth / 2 - MeasureText(rules.text, rules.fontSize) / 2)), rules.posY, rules.fontSize, colorInMenu);
			DrawText(credits.text, ((screenWidth / 2 - MeasureText(credits.text, credits.fontSize) / 2)), credits.posY, credits.fontSize, colorInMenu);
			DrawText(exit.text, ((screenWidth / 2 - MeasureText(exit.text, exit.fontSize) / 2)), exit.posY, exit.fontSize, colorInMenu);
			break;
		}
	}

	void drawGameContourn(Color colorChoice)
	{
		Color color = colorChoice;

		struct Line
		{
			Vector2 StartPos;
			Vector2 EndPos;
		};

		Line upperLine;
		Line lowerLine;
		Line leftLine;
		Line rightLine;

		upperLine.StartPos.y = static_cast<float>(screenLimit.up + contourLineThickness / 2);
		upperLine.StartPos.x = static_cast<float>(screenLimit.left);
		upperLine.EndPos.y = static_cast<float>(screenLimit.up + contourLineThickness / 2);
		upperLine.EndPos.x = static_cast<float>(screenLimit.right);
		//--------------------------------------------
		lowerLine.StartPos.y = static_cast<float>(screenLimit.down - contourLineThickness / 2);
		lowerLine.StartPos.x = static_cast<float>(screenLimit.left);
		lowerLine.EndPos.y = static_cast<float>(screenLimit.down - contourLineThickness / 2);
		lowerLine.EndPos.x = static_cast<float>(screenLimit.right);
		//--------------------------------------------
		leftLine.StartPos.y = static_cast<float>(screenLimit.up);
		leftLine.StartPos.x = static_cast<float>(screenLimit.left + contourLineThickness / 2);
		leftLine.EndPos.y = static_cast<float>(screenLimit.down);
		leftLine.EndPos.x = static_cast<float>(screenLimit.left + contourLineThickness / 2);
		//--------------------------------------------
		rightLine.StartPos.y = static_cast<float>(screenLimit.up);
		rightLine.StartPos.x = static_cast<float>(screenLimit.right - contourLineThickness / 2);
		rightLine.EndPos.y = static_cast<float>(screenLimit.down);
		rightLine.EndPos.x = static_cast<float>(screenLimit.right - contourLineThickness / 2);
		//--------------------------------------------

		DrawLineEx(upperLine.StartPos, upperLine.EndPos, contourLineThickness, color);
		DrawLineEx(lowerLine.StartPos, lowerLine.EndPos, contourLineThickness, color);
		DrawLineEx(leftLine.StartPos, leftLine.EndPos, contourLineThickness, color);
		DrawLineEx(rightLine.StartPos, rightLine.EndPos, contourLineThickness, color);
	}	

	void drawGameSpace()
	{
		pressEnter.text = "Press enter to go back to menu";

		pressPtoPause.text = "Press P to pause the game";

		pressEnter.posY = 30;

		pressPtoPause.posY = 394;

		pressEnter.fontSize = 26;

		pressPtoPause.fontSize = 26;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);
		DrawText(pressEnter.text, (screenWidth / 2 - MeasureText(pressEnter.text, pressEnter.fontSize) / 2), pressEnter.posY, pressEnter.fontSize, VIOLET);
		DrawText(pressPtoPause.text, (screenWidth / 2 - MeasureText(pressPtoPause.text, pressPtoPause.fontSize) / 2), pressPtoPause.posY, pressPtoPause.fontSize, VIOLET);
		score::drawPlayerPoints();
		drawElements();
	}

	void drawWinnerScreen()
	{
		const Color color = YELLOW;
		const short p1PosX = 410;
		const short p2PosX = 405;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);

		thePlayerHasWon.text = "The player   has won!";

		one.text = " 1";
		two.text = " 2";

		pressEnter.text = "Press enter to go back to menu";

		congratulations.text = "Congratulations!";

		thePlayerHasWon.posY = 125;

		one.posY = thePlayerHasWon.posY;
		two.posY = thePlayerHasWon.posY;

		pressEnter.posY = 355;

		congratulations.posY = 195;

		thePlayerHasWon.fontSize = 50;

		one.fontSize = thePlayerHasWon.fontSize;
		two.fontSize = thePlayerHasWon.fontSize;

		pressEnter.fontSize = 30;

		congratulations.fontSize = 75;

		DrawText(thePlayerHasWon.text, (screenWidth / 2 - MeasureText(thePlayerHasWon.text, thePlayerHasWon.fontSize) / 2), thePlayerHasWon.posY, thePlayerHasWon.fontSize, color);
		if (player1.points > player2.points)
			DrawText(one.text, p1PosX, one.posY, one.fontSize, player1.color);
		else
			DrawText(two.text, p2PosX, two.posY, two.fontSize, player2.color);

		DrawText(congratulations.text, (screenWidth / 2 - MeasureText(congratulations.text, congratulations.fontSize) / 2), congratulations.posY, congratulations.fontSize, color);

		DrawText(pressEnter.text, (screenWidth / 2 - MeasureText(pressEnter.text, pressEnter.fontSize) / 2), pressEnter.posY, pressEnter.fontSize, VIOLET);
	}

	void drawPauseScreen()
	{
		pause.text = "Pause";
		pressEnter.text = "Press enter to go back to menu";
		pressPtoContinue.text = "Press P to continue the game";

		pause.fontSize = 100;
		pressEnter.fontSize = 26;
		pressPtoContinue.fontSize = 26;

		pause.posY = 175;
		pressEnter.posY = 30;
		pressPtoContinue.posY = 394;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);
		DrawText(pause.text, (screenWidth / 2 - MeasureText(pause.text, pause.fontSize) / 2), pause.posY, pause.fontSize, YELLOW);
		DrawText(pressEnter.text, (screenWidth / 2 - MeasureText(pressEnter.text, pressEnter.fontSize) / 2), pressEnter.posY, pressEnter.fontSize, VIOLET);
		DrawText(pressPtoContinue.text, (screenWidth / 2 - MeasureText(pressPtoContinue.text, pressPtoContinue.fontSize) / 2), pressPtoContinue.posY, pressPtoContinue.fontSize, VIOLET);
	}

	void drawRules()
	{
		const short fontSize = 30;
		const Color color = YELLOW;
		const short posX = 260;

		short CapitalToLowerCase = 32;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);

		rulesText[firstLine].text = "The objetive of the game is to keep the ball";
		rulesText[secondLine].text = "on the game without it touching the right or";
		rulesText[thirdLine].text = "left limits of the screen.";
		rulesText[fourthLine].text = "Controls";
		rulesText[fifthLine].text = "1rst player:";
		rulesText[sixthLine].text = "2nd player:";
		rulesText[seventhLine].text = "The first player to reach 10 points wins.";
		rulesText[eighthLine].text = "Watch out for the powerups!";

		pressEnter.text = "Press enter to go back to menu";

		rulesText[firstLine].posY = 70;
		rulesText[secondLine].posY = 100;
		rulesText[thirdLine].posY = 130;
		rulesText[fourthLine].posY = 175;
		rulesText[fifthLine].posY = 205;
		rulesText[sixthLine].posY = 235;
		rulesText[seventhLine].posY = 265;
		rulesText[eighthLine].posY = 295;

		pressEnter.posY = 355;

		DrawText(rulesText[firstLine].text, ((screenWidth / 2 - MeasureText(rulesText[firstLine].text, fontSize) / 2)), rulesText[firstLine].posY, fontSize, color);
		DrawText(rulesText[secondLine].text, ((screenWidth / 2 - MeasureText(rulesText[secondLine].text, fontSize) / 2)), rulesText[secondLine].posY, fontSize, color);
		DrawText(rulesText[thirdLine].text, ((screenWidth / 2 - MeasureText(rulesText[thirdLine].text, fontSize) / 2)), rulesText[thirdLine].posY, fontSize, color);
		DrawText(rulesText[fourthLine].text, ((screenWidth / 2 - MeasureText(rulesText[fourthLine].text, fontSize) / 2)), rulesText[fourthLine].posY, fontSize, color);
		DrawText(rulesText[fifthLine].text, posX, rulesText[fifthLine].posY, fontSize, color);

		DrawText(FormatText("  %c - ", player1.up), posX + (MeasureText(rulesText[fifthLine].text, fontSize)), rulesText[fifthLine].posY, fontSize, color);
		DrawText(FormatText(" %c", player1.down), posX + (MeasureText(rulesText[fifthLine].text, fontSize)) + (MeasureText(FormatText(" %c - ", player1.up), fontSize)), rulesText[fifthLine].posY, fontSize, color);

		DrawText(rulesText[sixthLine].text, posX, rulesText[sixthLine].posY, fontSize, color);

		DrawText(FormatText("  %c - ", player2.up), posX + (MeasureText(rulesText[sixthLine].text, fontSize)), rulesText[sixthLine].posY, fontSize, color);
		DrawText(FormatText(" %c", player2.down), posX + (MeasureText(rulesText[sixthLine].text, fontSize)) + (MeasureText(FormatText(" %c - ", player2.up), fontSize)), rulesText[sixthLine].posY, fontSize, color);

		DrawText(rulesText[seventhLine].text, ((screenWidth / 2 - MeasureText(rulesText[seventhLine].text, fontSize) / 2)), rulesText[seventhLine].posY, fontSize, color);
		DrawText(rulesText[eighthLine].text, ((screenWidth / 2 - MeasureText(rulesText[eighthLine].text, fontSize) / 2)), rulesText[eighthLine].posY, fontSize, color);


		DrawText(pressEnter.text, ((screenWidth / 2 - MeasureText(pressEnter.text, fontSize) / 2)), pressEnter.posY, fontSize, VIOLET);
	}

	void drawOptions()
	{
		const Color colorSelected = MAGENTA;
		const Color color = YELLOW;

		short arrowLenght = 60;

		Vector2 mouse;

		mouse.x = static_cast<float>(GetMouseX());
		mouse.y = static_cast<float>(GetMouseY());

		controls.text = "Controls";
		paddleColor.text = "Paddle color";
		ballColor.text = "Ball color";
		gameMode.text = "Game mode";

		controlsAndArrow.text = "-> Controls";
		paddleColorAndArrow.text = "-> Paddle color";
		ballColorAndArrow.text = "-> Ball color";
		gameModeAndArrow.text = "-> Game mode";

		optionsText[firstLine].text = "Put your mouse over the characteristic you want to";
		optionsText[secondLine].text = "change and use A and D or the arrows to select the";
		optionsText[thirdLine].text = "option you want to change.";

		pressEnter.text = "Press enter to go back to menu";

		short posX = 100;

		controls.posY = 140;
		paddleColor.posY = 185;
		ballColor.posY = 230;
		gameMode.posY = 285;

		optionsText[firstLine].posY = 50;
		optionsText[secondLine].posY = 75;
		optionsText[thirdLine].posY = 100;

		pressEnter.posY = 355;

		controls.fontSize = 45;
		paddleColor.fontSize = 45;
		ballColor.fontSize = 45;
		gameMode.fontSize = 45;

		optionsText[firstLine].fontSize = 27;
		optionsText[secondLine].fontSize = 27;
		optionsText[thirdLine].fontSize = 27;

		pressEnter.fontSize = 30;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);

		DrawText(optionsText[firstLine].text, ((screenWidth / 2 - MeasureText(optionsText[firstLine].text, optionsText[firstLine].fontSize) / 2)), optionsText[firstLine].posY, optionsText[firstLine].fontSize, VIOLET);
		DrawText(optionsText[secondLine].text, ((screenWidth / 2 - MeasureText(optionsText[secondLine].text, optionsText[secondLine].fontSize) / 2)), optionsText[secondLine].posY, optionsText[secondLine].fontSize, VIOLET);
		DrawText(optionsText[thirdLine].text, ((screenWidth / 2 - MeasureText(optionsText[thirdLine].text, optionsText[thirdLine].fontSize) / 2)), optionsText[thirdLine].posY, optionsText[thirdLine].fontSize, VIOLET);
		DrawText(pressEnter.text, (screenWidth / 2 - MeasureText(pressEnter.text, pressEnter.fontSize) / 2), pressEnter.posY, pressEnter.fontSize, VIOLET);

		switch (currentPlaceInOptions)
		{
		case PLACEINOPTIONS::CONTROLS:
			DrawText(controlsAndArrow.text, posX - arrowLenght, controls.posY, controls.fontSize, colorSelected);
			DrawText(paddleColor.text, posX, paddleColor.posY, paddleColor.fontSize, color);
			DrawText(ballColor.text, posX, ballColor.posY, ballColor.fontSize, color);
			DrawText(gameMode.text, posX, gameMode.posY, gameMode.fontSize, color);
			break;
		case PLACEINOPTIONS::PADDLECOLOR:
			DrawText(controls.text, posX, controls.posY, controls.fontSize, color);
			DrawText(paddleColorAndArrow.text, posX - arrowLenght, paddleColor.posY, paddleColor.fontSize, colorSelected);
			DrawText(ballColor.text, posX, ballColor.posY, ballColor.fontSize, color);
			DrawText(gameMode.text, posX, gameMode.posY, gameMode.fontSize, color);
			break;
		case PLACEINOPTIONS::BALLCOLOR:
			DrawText(controls.text, posX, controls.posY, controls.fontSize, color);
			DrawText(paddleColor.text, posX, paddleColor.posY, paddleColor.fontSize, color);
			DrawText(ballColorAndArrow.text, posX - arrowLenght, ballColor.posY, ballColor.fontSize, colorSelected);
			DrawText(gameMode.text, posX, gameMode.posY, gameMode.fontSize, color);
			break;
		case PLACEINOPTIONS::GAMEMODE:
			DrawText(controls.text, posX, controls.posY, controls.fontSize, color);
			DrawText(paddleColor.text, posX, paddleColor.posY, paddleColor.fontSize, color);
			DrawText(ballColor.text, posX, ballColor.posY, ballColor.fontSize, color);
			DrawText(gameModeAndArrow.text, posX - arrowLenght, gameMode.posY, gameMode.fontSize, colorSelected);
			break;
		case PLACEINOPTIONS::NONE:
			DrawText(controls.text, posX, controls.posY, controls.fontSize, color);
			DrawText(paddleColor.text, posX, paddleColor.posY, paddleColor.fontSize, color);
			DrawText(ballColor.text, posX, ballColor.posY, ballColor.fontSize, color);
			DrawText(gameMode.text, posX, gameMode.posY, gameMode.fontSize, color);
			break;
		default:
			break;
		}
	}

	void drawChoicesInOptionsScreen()
	{
		const Color color = YELLOW;
		const short fontSize = 35;

		p1Up.text = "P1 up:";
		p1Down.text = "P1 down:";
		p2Up.text = "P2 up:";
		p2Down.text = "P2 down:";

		p1PaddleColorMovement.text = "P1(A - D):";
		p2PaddleColorMovement.text = "P2(<- - ->):";
		swatch.text = "swatch";

		playerVSplayer.text = "Player vs Player";
		playerVScomputer.text = "Player vs Computer";

		ballColorMovement.text = "Color:";

		short maxControlsOptions = 4;
		short maxPaddleColorOptions = 22;
		short maxBallColorOptions = 22;
		short maxGameModeOptions = 2;

		short posX = 420;
		short posY = 200;

		short fistCapitalLetter = 65;
		short lastCapitalLetter = 90;
		short firstLowerCaseLetter = 97;
		short lastLowerCaseLetter = 122;
		
		short CapitalToLowerCase = 32;

		int keySelected;

		switch (currentPlaceInOptions)
		{
		case PLACEINOPTIONS::CONTROLS:
			if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
				movement_through_game::setUpMovementForConfigurations(maxControlsOptions);
			switch (configOption)
			{
			case 1:
				DrawText(p1Up.text, posX, posY, fontSize, color);
				DrawText(FormatText("  %c", player1.up), posX + MeasureText(p1Up.text, fontSize), posY, fontSize, color);
				
				if (movement_through_game::checkIfAnyKeyIsPressed())
				{
					keySelected = GetKeyPressed() - CapitalToLowerCase;

					if (GetKeyPressed() != player1.down  && GetKeyPressed() != player2.up && GetKeyPressed() != player2.down)
						player1.up = keySelected;
				}
				
				break;
			case 2:
				DrawText(p1Down.text, posX, posY, fontSize, color);
				DrawText(FormatText("  %c", player1.down), posX + MeasureText(p1Down.text, fontSize), posY, fontSize, color);

				if (movement_through_game::checkIfAnyKeyIsPressed())
				{
					keySelected = GetKeyPressed() - CapitalToLowerCase;

					if (GetKeyPressed() != player1.up && GetKeyPressed() != player2.up && GetKeyPressed() != player2.down)
						player1.down = keySelected;
				}
				break;
			case 3:
				DrawText(p2Up.text, posX, posY, fontSize, color);
				DrawText(FormatText("  %c", player2.up), posX + MeasureText(p2Up.text, fontSize), posY, fontSize, color);

				if (movement_through_game::checkIfAnyKeyIsPressed())
				{
					keySelected = GetKeyPressed() - CapitalToLowerCase;

					if (GetKeyPressed() != player2.down && GetKeyPressed() != player1.up && GetKeyPressed() != player1.down)
						player2.up = keySelected;
				}
				break;
			case 4:
				DrawText(p2Down.text, posX, posY, fontSize, color);
				DrawText(FormatText("  %c", player2.down), posX + MeasureText(p2Down.text, fontSize), posY, fontSize, color);

				if (movement_through_game::checkIfAnyKeyIsPressed())
				{
					keySelected = GetKeyPressed() - CapitalToLowerCase;

					if (GetKeyPressed() != player2.up && GetKeyPressed() != player1.up && GetKeyPressed() != player1.down)
						player2.down = keySelected;
				}
				break;
			default:
				break;
			}
			break;
		case PLACEINOPTIONS::PADDLECOLOR:
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_D) || IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
			{
				movement_through_game::setUpMovementForColorOptions(maxPaddleColorOptions);
				players::setPaddleP1ColorSelected();
				players::setPaddleP2ColorSelected();
			}

			DrawText(p1PaddleColorMovement.text, posX, posY, fontSize, color);
			DrawText(p2PaddleColorMovement.text, posX, posY + fontSize, fontSize, color);
			DrawText(swatch.text, posX + MeasureText(p1PaddleColorMovement.text, fontSize) + MeasureText(p1PaddleColorMovement.text, fontSize)/7, posY, fontSize, player1.color);
			DrawText(swatch.text, posX + MeasureText(p2PaddleColorMovement.text, fontSize) + MeasureText(p2PaddleColorMovement.text, fontSize)/7, posY + fontSize, fontSize, player2.color);
			break;
		case PLACEINOPTIONS::BALLCOLOR:
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_D) || IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
			{
				movement_through_game::setUpMovementForBallColorOptions(maxBallColorOptions);
				ball_logic::setBallColorSelected();
			}
			DrawText(ballColorMovement.text, posX, posY, fontSize, color);
			DrawText(swatch.text, posX + MeasureText(ballColorMovement.text, fontSize) + MeasureText(ballColorMovement.text, fontSize) / 7, posY, fontSize, ball.color);
			break;
		case PLACEINOPTIONS::GAMEMODE:
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_D) || IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT))
				movement_through_game::setUpMovementForGameMode(maxGameModeOptions);
			switch (global_vars::gameModeOption)
			{
			case 1:
				DrawText(playerVSplayer.text, posX, posY, fontSize, color);
				global_vars::PvsP = true;
				break;
			case 2:
				DrawText(playerVScomputer.text, posX, posY, fontSize, color);
				global_vars::PvsP = false;
				break;
			default:
				break;
			}
			break;
		case PLACEINOPTIONS::NONE:
		default:
			break;
		}
	}

	void drawCredits()
	{
		const Color color = YELLOW;

		ClearBackground(PURPLE);
		scenes_draw::drawGameContourn(VIOLET);

		creditsText[firstLine].text = "Done by Guillermina Manzano";
		creditsText[secondLine].text = "using the \"raylib\" library";
		creditsText[thirdLine].text = "raylib web site";
		creditsText[fourthLine].text = "https://www.raylib.com/index.html";

		pressEnter.text = "Press enter to go back to menu";

		creditsText[firstLine].posY = 80;
		creditsText[secondLine].posY = 135;
		creditsText[thirdLine].posY = 210;
		creditsText[fourthLine].posY = 270;

		pressEnter.posY = 355;

		creditsText[firstLine].fontSize = 50;
		creditsText[secondLine].fontSize = 45;
		creditsText[thirdLine].fontSize = 50;
		creditsText[fourthLine].fontSize = 40;

		pressEnter.fontSize = 30;

		DrawText(creditsText[firstLine].text, ((screenWidth / 2 - MeasureText(creditsText[firstLine].text, creditsText[firstLine].fontSize) / 2)), creditsText[firstLine].posY, creditsText[firstLine].fontSize, color);
		DrawText(creditsText[secondLine].text, ((screenWidth / 2 - MeasureText(creditsText[secondLine].text, creditsText[secondLine].fontSize) / 2)), creditsText[secondLine].posY, creditsText[secondLine].fontSize, color);
		DrawText(creditsText[thirdLine].text, ((screenWidth / 2 - MeasureText(creditsText[thirdLine].text, creditsText[thirdLine].fontSize) / 2)), creditsText[thirdLine].posY, creditsText[thirdLine].fontSize, color);
		DrawText(creditsText[fourthLine].text, ((screenWidth / 2 - MeasureText(creditsText[fourthLine].text, creditsText[fourthLine].fontSize) / 2)), creditsText[fourthLine].posY, creditsText[fourthLine].fontSize, MAGENTA);

		DrawText(pressEnter.text, ((screenWidth / 2 - MeasureText(pressEnter.text, pressEnter.fontSize) / 2)), pressEnter.posY, pressEnter.fontSize, VIOLET);
	}
}