#ifndef SCENES_H
#define SCENES_H

namespace scenes
{
	namespace menu
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}

	namespace match
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}

	namespace options
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}

	namespace rules
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}

	namespace credits
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}
}

#endif //SCENES_H