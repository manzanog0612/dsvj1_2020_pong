#ifndef SUB_SCENES_H
#define SUB_SCENES_H

namespace sub_scenes
{
	namespace score
	{
		void resetGame();

		void setScore();

		void restartScore();

		bool checkIfMatchEnded();

		void drawPlayerPoints();

		void drawMatchPoint();
	}

	namespace movement_through_game
	{
		void selectMenuChoiceToHightlight();

		void selectOptionsChoiceToHightlight();

		void goToMenu();

		void goToPlaceSelected();

		void setUpMovementForGameMode(short maxOption);

		void setUpMovementForConfigurations(short maxOption);

		void setUpMovementForColorOptions(short maxOption);

		void setUpMovementForBallColorOptions(short maxOption);

		bool checkIfAnyKeyIsPressed();
	}
}

#endif //SUB_SCENES_H