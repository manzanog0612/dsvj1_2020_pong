#include "game/scenes/scenes.h"

#include "raylib.h"

#include "elements/elements_properties.h"
#include "functions library/movement_library.h"

#include "functions library/global_vars_library.h"
#include "functions library/global_drawing_vars_library.h"
#include "game/scenes/sub_scenes.h"
#include "game/scenes/scenes_draw.h"
#include "elements/elements.h"
#include "functions library/powerups_library.h"

using namespace global_vars;
using namespace global_drawing_vars;
using namespace sub_scenes;
using namespace scenes_draw;
using namespace elements;
using namespace powerup;

namespace scenes
{
	bool scoreSetted = false;

	namespace menu
	{
		void initialization()
		{
			title.text = "Pong";
			title.posY = 50;
			title.fontSize = 100;

			currentPlaceInGame = PLACEINGAME::MENU;
			currentPlaceInOptions = PLACEINOPTIONS::NONE;
			currentPlaceInOptions = PLACEINOPTIONS::NONE;
			InitWindow(screenWidth, screenHeight, title.text);
			SetTargetFPS(120);
		}

		void input()
		{
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				switch (futurePlaceInGame)
				{
				case PLACEINGAME::MATCH:
					score::restartScore();
					score::resetGame();
					scoreSetted = false;
					inGame = true;
					pauseMatch = false;
					powerups_logic::setTimeForPowerUp();
				case PLACEINGAME::MENU:
				case PLACEINGAME::OPTIONS:
				case PLACEINGAME::RULES:
				case PLACEINGAME::CREDITS:
				case PLACEINGAME::EXIT:
					movement_through_game::goToPlaceSelected();
					break;
				case PLACEINGAME::NONE:
				default:
					break;
				}
			}
		}

		void update()
		{
			movement_through_game::selectMenuChoiceToHightlight();
		}

		void draw()
		{
			drawMenu();
		}

		void deinitialization()
		{

		}
	}

	namespace match
	{
		void initialization()
		{
			ball.speed.x = 2;
			ball.speed.y = 2;
			firstElementsDefinitions();
			ball_logic::randomizeBallDirection();
			powerups_logic::powerUpInitialization();
		}

		void input()
		{
			if (inGame)
			{
				if (!pauseMatch)
					players::setPlayersMovement();
				switch (pauseMatch)
				{
				case false:
					if (IsKeyPressed(KEY_P))
						pauseMatch = true;
					break;
				case true:
					if (IsKeyPressed(KEY_P))
						pauseMatch = false;
					break;
				}
			}
			if (IsKeyPressed(KEY_ENTER))
			{
				inGame = false;
				movement_through_game::goToMenu();
				score::restartScore();
			}
		}

		void update()
		{
			if (!pauseMatch)
			{
				if (inGame)
				{
					ball_logic::setBallMovement();
					ball_logic::setLastBallTouch();
					score::setScore();
					if (powerups_logic::getTimeForNextPowerUp() < GetTime())
						powerups_logic::activatePowerUpView();
					if (powerups_logic::verifyPowerUpCollision())
					{
						powerups_logic::setUpPowerUp();
						powerUpTimeDurationSet = true;
					}
					if (powerUpEndingTime < GetTime() && powerUpTimeDurationSet)
					{
						powerups_logic::deactivatePowerUpsByTime();
					}
					powerups_logic::activatePowerUp();
				}
				if (score::checkIfMatchEnded() && !scoreSetted)
				{
					inGame = false;
					players::addMatchPointToPlayer();
					scoreSetted = true;
				}
			}
		}

		void draw()
		{
			if (!pauseMatch)
			{
				if (!inGame)
				{
					drawWinnerScreen();
				}
				else
				{
					if (powerups_logic::getTimeForNextPowerUp() < GetTime())
						powerups_logic::drawPowerUp();
					drawGameSpace();
					score::drawMatchPoint();
				}
			}
			else
			{
				drawPauseScreen();
			}
		}

		void deinitialization()
		{
			
		}
	}

	namespace options
	{
		void initialization()
		{
			players::playerMovementDefaultDefinition();
		}

		void input()
		{
			movement_through_game::selectOptionsChoiceToHightlight();
			if (IsKeyPressed(KEY_ENTER))
				movement_through_game::goToMenu();
		}

		void update()
		{

		}

		void draw()
		{
			scenes_draw::drawOptions();
			scenes_draw::drawChoicesInOptionsScreen();
		}

		void deinitialization()
		{

		}
	}

	namespace rules
	{
		void initialization()
		{

		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				movement_through_game::goToMenu();
		}

		void update()
		{

		}

		void draw()
		{
			drawRules();
		}

		void deinitialization()
		{

		}
	}

	namespace credits
	{
		void initialization()
		{

		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				movement_through_game::goToMenu();
		}

		void update()
		{

		}

		void draw()
		{
			drawCredits();
		}

		void deinitialization()
		{

		}
	}
}