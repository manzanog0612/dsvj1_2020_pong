#include "game/game.h"

#include <iostream>

#include "raylib.h"

#include "functions library/global_vars_library.h"
#include "game/scenes/scenes.h"

using namespace scenes;
using namespace global_vars;
using namespace movement_library;

namespace game
{
	void initialization()
	{
		menu::initialization();
		match::initialization();
		options::initialization();
		rules::initialization();
		credits::initialization();
	}

	void input()
	{
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::input();		break;
		case PLACEINGAME::MATCH:	match::input();		break;
		case PLACEINGAME::OPTIONS:	options::input();	break;
		case PLACEINGAME::RULES:	rules::input();		break;
		case PLACEINGAME::CREDITS:	credits::input();	break;
		case PLACEINGAME::EXIT:
		case PLACEINGAME::NONE:
		default:
			break;
		}
	}

	void update()
	{
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::update();			break;
		case PLACEINGAME::MATCH:	match::update();		break;
		case PLACEINGAME::OPTIONS:	options::update();		break;
		case PLACEINGAME::RULES:	rules::update();		break;
		case PLACEINGAME::CREDITS:	credits::update();		break;
		case PLACEINGAME::EXIT:		playingGame = false;	break;
		case PLACEINGAME::NONE:
		default:
			break;
		}
	}

	void draw()
	{
		BeginDrawing();
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::draw();		break;
		case PLACEINGAME::MATCH:	match::draw();		break;
		case PLACEINGAME::OPTIONS:	options::draw();	break;
		case PLACEINGAME::RULES:	rules::draw();		break;
		case PLACEINGAME::CREDITS:	credits::draw();	break;
		case PLACEINGAME::EXIT:
		case PLACEINGAME::NONE:
		default:
			break;
		}
		EndDrawing();
	}

	void deinitialization()
	{
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::deinitialization();		break;
		case PLACEINGAME::MATCH:	match::deinitialization();		break;
		case PLACEINGAME::OPTIONS:	options::deinitialization();	break;
		case PLACEINGAME::RULES:	rules::deinitialization();		break;
		case PLACEINGAME::CREDITS:	credits::deinitialization();	break;
		case PLACEINGAME::EXIT:
		case PLACEINGAME::NONE:
		default:
			break;
		}
	}

	void run()
	{
		game::initialization();

		while (!WindowShouldClose() && playingGame)
		{
			game::input();
			game::update();
			game::draw();
		}
	}
}